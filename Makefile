all: bin/viterbi_jstat_search bin/viterbi_jstat_search_single

bin/viterbi_jstat_search: viterbi_jstat_search.cu viterbi.cu viterbi.h
	nvcc -O3 -arch=compute_60 --compiler-options -Wall -dc -m64 -o viterbi_jstat_search.o -c viterbi_jstat_search.cu
	nvcc -O3 -arch=compute_60 --compiler-options -Wall -dc -m64 -o viterbi.o -c viterbi.cu
	nvcc -O3 -arch=compute_60 --compiler-options -Wall -m64 -o bin/viterbi_jstat_search viterbi_jstat_search.o viterbi.o -lcufft_static -lculibos -lm
#	nvcc viterbi_jstat_search.cu viterbi.cu -o viterbi_jstat_search -arch=compute_35 -g -lcufft_static -lm -lculibos -G -gencode arch=compute_35,\"code=sm_35\"
viterbi_bessel_search: viterbi_bessel_search.cu viterbi.cu
	nvcc viterbi_bessel_search.cu viterbi.cu -o viterbi_bessel_search -arch=compute_20 -g -lcufft -lm -G
viterbi_fstat_search: viterbi_fstat_search.cu viterbi.cu
	nvcc viterbi_fstat_search.cu viterbi.cu -o viterbi_fstat_search -arch=compute_20 -g -lcufft -lm -G

bin/viterbi_jstat_search_single: viterbi_jstat_search.cu viterbi.cu viterbi.h
	nvcc -O3 --compiler-options -Wall -dc -m64 -o viterbi_jstat_search_single.o -c viterbi_jstat_search.cu -DSINGLE_PRECISION
	nvcc -O3 --compiler-options -Wall -dc -m64 -o viterbi_single.o -c viterbi.cu -DSINGLE_PRECISION
	nvcc -O3 --compiler-options -Wall -m64 -o bin/viterbi_jstat_search_single viterbi_jstat_search_single.o viterbi_single.o -lcufft_static -lculibos -lm -DSINGLE_PRECISION
