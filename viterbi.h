#ifndef VITERBI_H
#define VITERBI_H

#include <cuComplex.h>

#ifdef SINGLE_PRECISION
#define vit_cuRealType              float
#define vit_cuComplexType           cuFloatComplex
#define vit_cufftComplexType        cufftComplex
#define vit_cuCmul                  cuCmulf
#define vit_cufftCallbackLoadXXX    cufftCallbackLoadC
#define vit_CUFFT_X2X               CUFFT_C2C
#define vit_CUFFT_CB_LOAD_TYPE      CUFFT_CB_LD_COMPLEX
#define vit_cufftExecX2X            cufftExecC2C
#else
#define vit_cuRealType              double
#define vit_cuComplexType           cuDoubleComplex
#define vit_cufftComplexType        cufftDoubleComplex
#define vit_cuCmul                  cuCmul
#define vit_cufftCallbackLoadXXX    cufftCallbackLoadZ
#define vit_CUFFT_X2X               CUFFT_Z2Z
#define vit_CUFFT_CB_LOAD_TYPE      CUFFT_CB_LD_COMPLEX_DOUBLE
#define vit_cufftExecX2X            cufftExecZ2Z
#endif

typedef struct trellis_entry_t
{
    double loglike;
    int backptr;
} trellis_entry_t;

void usage(const char * progname);
void viterbi_run(double *obs_loglike_gpu_data, trellis_entry_t *trellis_gpu_data, int ol_block_size, int trellis_block_size, int ol_offset, int obs_len);
double viterbi_results(
		trellis_entry_t *trellis_gpu_data,
		int block_size,
		int obs_len,
		double best_score,
		int *path,
		double *sampled_scores,
		int num_sampled_scores,
		int **paths_above_threshold,
		double **path_scores_above_threshold,
		double threshold,
		int *num_threshold_scores_returned,
		double *score,
		int *num_llthreshold_scores_returned,
		double **lls_above_threshold,
		double llthreshold,
		trellis_entry_t *best_trellis);
void cuda_checked_malloc(void **ptr, size_t sz);
void cuda_checked_malloc_host(void **ptr, size_t sz);

void load_CFSv2_atoms_freqs(const char *fstat_path, const int block_size, double *top_freq, double *central_freq, double *bottom_freq);
void load_CFSv2_atoms(
	// Meta
	const char *atoms_dir,
	int block_size,
	int *obs_len,
	// Atoms
	cuDoubleComplex **fa_data,
	cuDoubleComplex **fb_data,
	// Antenna pattern &c
	double **Ahats,
	double **Bhats,
	double **Chats,
	double **Dhats,
	int *startGPStime,
	int *tblock
);

void load_CJS_atoms(
	const char *cjs_pattern,
	int block_size,
	int *obs_len,
	const int null_atom_count,
	cuDoubleComplex **fa_data,
	cuDoubleComplex **fb_data,
	double **Ahats,
	double **Bhats,
	double **Chats,
	double **Dhats,
	double *top_freq,
	double *central_freq,
	double *bottom_freq,
	double **middle_times
);
#endif
