#include <string.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <complex.h>
#include <glob.h>
#include <libgen.h>
#include <cuda.h>
#include <cufft.h>
#include <cufftXt.h>
#include <cuComplex.h>
#include <errno.h>
#include <stdarg.h>

#include "viterbi.h"

static FILE *checked_fopen(const char *const mode, const char *const format_str, ...);

struct __attribute__((__packed__)) meta
{
    unsigned int time;
    double asq;
    double bsq;
    double ab;
};

__global__ void jstat_convolve(vit_cuComplexType *fa_data, vit_cuComplexType *fb_data, vit_cuComplexType *filter, int block_size, int num_points)
{
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
	if(idx >= num_points) return;
    fa_data[idx] = vit_cuCmul(fa_data[idx], filter[idx]);
    fa_data[idx] = (vit_cuComplexType) {fa_data[idx].x/block_size, fa_data[idx].y/block_size};
    fb_data[idx] = vit_cuCmul(fb_data[idx], filter[idx]);
    fb_data[idx] = (vit_cuComplexType) {fb_data[idx].x/block_size, fb_data[idx].y/block_size};
}

typedef struct _conv_cb_params { vit_cuComplexType *filter; int block_size; } conv_cb_params;

__device__ vit_cufftComplexType convolve_cb(void *a, size_t idx, void *cb_info, void *shared_mem)
{
    conv_cb_params *params = (conv_cb_params *)cb_info;
    vit_cuComplexType res = vit_cuCmul(((vit_cuComplexType *)a)[idx], (params->filter)[idx]);
    return (vit_cufftComplexType) {res.x/params->block_size, res.y/params->block_size};
}

__device__ vit_cufftCallbackLoadXXX convolve_cb_ptr = convolve_cb;

__global__ void jstat(vit_cuComplexType *fa_data, vit_cuComplexType *fb_data, double *jstats,
                    double Ahat, double Bhat, double Chat, double Dhat , int t, int block_size)
{
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    if(idx >= block_size) { return; }
    int jstat_idx;
    // Need to exchange the left and right halves of the jstat vector (cf. fftshift in MATLAB)
    if(idx >= block_size/2 + (block_size % 2))
        jstat_idx = idx - block_size/2 - (block_size % 2);
    else
        jstat_idx = idx + block_size/2;
    jstats[jstat_idx] = 2/Dhat*(
                Bhat*(fa_data[idx].x*fa_data[idx].x + fa_data[idx].y*fa_data[idx].y)
                + Ahat*(fb_data[idx].x*fb_data[idx].x + fb_data[idx].y*fb_data[idx].y)
                - 2*Chat*(fa_data[idx].x*fb_data[idx].x + fa_data[idx].y*fb_data[idx].y));

}

__global__ void compute_base_filter(vit_cuComplexType *jstat_filter, double a0, double bottom_freq, double top_freq, double central_freq, double P, int M, int block_size, double bins_per_hz)
{
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    if (idx > (M-1)/2)
    {
        return;
    }

    int n_above = -idx;
    int n_below = idx;

    double freq_above = central_freq + idx/P;
    double freq_below = central_freq - idx/P;
    double raw_bin_above = (freq_above - bottom_freq)*bins_per_hz;
    double raw_bin_below = (freq_below - bottom_freq)*bins_per_hz;

    int comb_offset_above = lrint(raw_bin_above);
    int comb_offset_below = lrint(raw_bin_below);

    jstat_filter[comb_offset_above] = (vit_cuComplexType) {
        jn(-n_above, 2*M_PI*central_freq*a0),
        0};
    // jn(-n, x) = (-1)^n*jn(n, x), so if n is odd we flip the sign
    if(-n_above % 2 == 1)
    {
        jstat_filter[comb_offset_above].x = -jstat_filter[comb_offset_above].x;
        jstat_filter[comb_offset_above].y = -jstat_filter[comb_offset_above].y;
    }
    jstat_filter[comb_offset_below] = (vit_cuComplexType) {
       jn(n_below, 2*M_PI*central_freq*a0),
        0};

}
__global__ void phase_adjust_filter(vit_cuComplexType *jstat_filter, vit_cuComplexType *base_jstat_filter, double bottom_freq, double top_freq, double central_freq, double phase_i, double P, int M, int block_size, double bins_per_hz)
{
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    if (idx > (M-1)/2)
    {
        return;
    }

    int n_above = -idx;
    int n_below = idx;

    double freq_above = central_freq + idx/P;
    double freq_below = central_freq - idx/P;
    double raw_bin_above = (freq_above - bottom_freq)*bins_per_hz;
    double raw_bin_below = (freq_below - bottom_freq)*bins_per_hz;

    int comb_offset_above = lrint(raw_bin_above);
    int comb_offset_below = lrint(raw_bin_below);

    vit_cuComplexType phase_factor_above = (vit_cuComplexType) { cos(-n_above*phase_i), sin(-n_above*phase_i) };
    vit_cuComplexType phase_factor_below = (vit_cuComplexType) { cos(-n_below*phase_i), sin(-n_below*phase_i) };

    jstat_filter[comb_offset_above] = vit_cuCmul(base_jstat_filter[comb_offset_above], phase_factor_above);
    jstat_filter[comb_offset_below] = vit_cuCmul(base_jstat_filter[comb_offset_below], phase_factor_below);

}

__global__ void compute_filter_atomic_orbitTp(vit_cuComplexType *jstat_filter, double bottom_freq, double top_freq, double central_freq, double a0, double *middle_times, double orbitTp, double P, int highest_sideband_idx, int block_size, int obs_len, double bins_per_hz)
{
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    if (idx >= (highest_sideband_idx+1)*obs_len)
    {
        return;
    }
    int block = idx/(highest_sideband_idx+1);

    idx = idx % (highest_sideband_idx + 1);
    int n_above = -idx;
    int n_below = idx;
   
    const double Tasc = ( (middle_times[block] - orbitTp ) );
    double dummy;
    double phase_i = modf(Tasc / P, &dummy);
    phase_i -= 0.5;
    if(phase_i < 0) phase_i += 1.;
    phase_i *= 2*M_PI;

    double freq_above = central_freq + idx/P;
    double freq_below = central_freq - idx/P;
    double raw_bin_above = (freq_above - bottom_freq)*bins_per_hz;
    double raw_bin_below = (freq_below - bottom_freq)*bins_per_hz;

    int comb_offset_above = lrint(raw_bin_above);
    int comb_offset_below = lrint(raw_bin_below);

    if(comb_offset_above >= block_size || comb_offset_below < 0)
        return;

    vit_cuComplexType phase_factor_above = (vit_cuComplexType) { cos(-n_above*phase_i), sin(-n_above*phase_i) };
    vit_cuComplexType phase_factor_below = (vit_cuComplexType) { cos(-n_below*phase_i), sin(-n_below*phase_i) };

    vit_cuComplexType jstat_filter_above = (vit_cuComplexType) {
        jn(-n_above, 2*M_PI*central_freq*a0) * (n_above != n_below),
        0};
    // jn(-n, x) = (-1)^n*jn(n, x), so if n is odd we flip the sign
    if(-n_above % 2 == 1)
    {
        jstat_filter_above.x = -jstat_filter_above.x;
        jstat_filter_above.y = -jstat_filter_above.y;
    }

    vit_cuComplexType jstat_filter_below = (vit_cuComplexType) {
       jn(n_below, 2*M_PI*central_freq*a0),
        0};

    jstat_filter_above = vit_cuCmul(jstat_filter_above, phase_factor_above);
    jstat_filter_below = vit_cuCmul(jstat_filter_below, phase_factor_below);
    vit_cuRealType *jstat_filter_above_re = (vit_cuRealType *)(jstat_filter + block*block_size + comb_offset_above);
    vit_cuRealType *jstat_filter_above_im = (vit_cuRealType *)(jstat_filter + block*block_size + comb_offset_above) + 1;
    vit_cuRealType *jstat_filter_below_re = (vit_cuRealType *)(jstat_filter + block*block_size + comb_offset_below);
    vit_cuRealType *jstat_filter_below_im = (vit_cuRealType *)(jstat_filter + block*block_size + comb_offset_below) + 1;

    atomicAdd(jstat_filter_above_re, jstat_filter_above.x);
    atomicAdd(jstat_filter_above_im, jstat_filter_above.y);
    atomicAdd(jstat_filter_below_re, jstat_filter_below.x);
    atomicAdd(jstat_filter_below_im, jstat_filter_below.y);
}

__global__ void compute_filter_atomic_phase(vit_cuComplexType *jstat_filter, double bottom_freq, double top_freq, double central_freq, double a0, double tblock, double starting_phase, double P, int highest_sideband_idx, int block_size, int obs_len, double bins_per_hz)
{
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    if (idx >= (highest_sideband_idx+1)*obs_len)
    {
        return;
    }
    int block = idx/(highest_sideband_idx+1);

    idx = idx % (highest_sideband_idx + 1);
    int n_above = -idx;
    int n_below = idx;
     
    double phase_i = starting_phase + 2*M_PI*(block*tblock)/P;


    double freq_above = central_freq + idx/P;
    double freq_below = central_freq - idx/P;
    double raw_bin_above = (freq_above - bottom_freq)*bins_per_hz;
    double raw_bin_below = (freq_below - bottom_freq)*bins_per_hz;

    int comb_offset_above = lrint(raw_bin_above);
    int comb_offset_below = lrint(raw_bin_below);

    if(comb_offset_above >= block_size || comb_offset_below < 0)
        return;

    vit_cuComplexType phase_factor_above = (vit_cuComplexType) { cos(-n_above*phase_i), sin(-n_above*phase_i) };
    vit_cuComplexType phase_factor_below = (vit_cuComplexType) { cos(-n_below*phase_i), sin(-n_below*phase_i) };

    vit_cuComplexType jstat_filter_above = (vit_cuComplexType) {
        jn(-n_above, 2*M_PI*central_freq*a0) * (n_above != n_below),
        0};
    // jn(-n, x) = (-1)^n*jn(n, x), so if n is odd we flip the sign
    if(-n_above % 2 == 1)
    {
        jstat_filter_above.x = -jstat_filter_above.x;
        jstat_filter_above.y = -jstat_filter_above.y;
    }

    vit_cuComplexType jstat_filter_below = (vit_cuComplexType) {
       jn(n_below, 2*M_PI*central_freq*a0),
        0};

    jstat_filter_above = vit_cuCmul(jstat_filter_above, phase_factor_above);
    jstat_filter_below = vit_cuCmul(jstat_filter_below, phase_factor_below);
    vit_cuRealType *jstat_filter_above_re = (vit_cuRealType *)(jstat_filter + block*block_size + comb_offset_above);
    vit_cuRealType *jstat_filter_above_im = (vit_cuRealType *)(jstat_filter + block*block_size + comb_offset_above) + 1;
    vit_cuRealType *jstat_filter_below_re = (vit_cuRealType *)(jstat_filter + block*block_size + comb_offset_below);
    vit_cuRealType *jstat_filter_below_im = (vit_cuRealType *)(jstat_filter + block*block_size + comb_offset_below) + 1;

    atomicAdd(jstat_filter_above_re, jstat_filter_above.x);
    atomicAdd(jstat_filter_above_im, jstat_filter_above.y);
    atomicAdd(jstat_filter_below_re, jstat_filter_below.x);
    atomicAdd(jstat_filter_below_im, jstat_filter_below.y);
}


// Print a usage message.
void usage(const char *const progname) {
	printf("%s -- GPU-enabled version of the J-statistic search\n"
"The following options are recognised:\n"
" --atoms=...   -- Directory containing the F-statistic atoms, such that\n"
"                  they will be found with glob patterns\n"
"                  effatoms-*-L1_{Fa,Fb,meta}.bin\n"
" --cjs_atoms=dir/xyz-%%d -- A sprintf(2)-style pattern for the location of\n"
"                  the atoms, stored in ComputeJStatistic style. The block\n"
"                  number will be substituted for '%%d'\n"
" --null_atoms= -- allow this many atom files to be missing\n"
" --start_time  -- start GPS time (only needed with --cjs_atoms)\n"
" --tblock      -- block length (only needed with --cjs_atoms)\n"
"\n"
" --central_a0= -- These three parameters set the search space for a sin i.\n"
" --a0_band=       The search is conducted in the region [central_a0 - a0_band/2,\n"
" --a0_bins=       central_a0 + a0_band/2] divided into a0_bins bins.\n"
"\n"
" --central_P   -- Orbital period of the binary [sec] (also recognises --P)\n"
" --P_band      -- Search parameters for orbital period\n"
" --P_bins      -- \n"
"\n"
" --central_phase= -- The search parameters for the orbital phase. [rad]\n"
" --phase_band=       (Specify either phase or orbitTp)\n"
" --phase_bins=\n"
"\n"
" --central_orbitTp= -- The search parameters for the time of periapsis\n"
" --orbitTp_band=       (defined the same way as in Makefakedata_v4)\n"
" --orbitTp_bins=       (Specify either phase or orbitTp)\n"
"\n"
" --block_size= -- Number of frequency bins per block.\n"
" --ignore_wings= -- Ignore this many frequency bins on each side of the\n"
"                  frequency block for the Viterbi search and the output. This\n"
"                  is useful to avoid sensitivity loss near the band edge.\n"
" --fstat=      -- An exemplar F-statistic file produced by ComputeFStatistic\n"
"                  for this search (used to determine frequency band)\n"
" --out_prefix= -- String to prefix output files with [default=gpusearch]\n"
" --obs_len=    -- Total observing timesteps N_T [default=guess from atoms]\n"
" --use_callback -- Use CUDA callbacks to improve performance [default=true]\n"
" --print_ll    -- Print loglikelihoods (not just the score) [default=false]\n"
" --threshold   -- Print all paths with scores exceeding this threshold [default=DBL_MAX]\n"
" --ll_threshold -- Print log-likelihoods exceeding this [default=DBL_MAX]\n"
" --dont_include_P_in_summary_file -- If specified, don't include P in the\n"
"                  summary file. Default false, unless --P is specified\n"
" --help        -- Display this usage note\n"
"\n"
"The J-statistic technique itself is described in:\n"
"  Suvorova et al, Physical Review D vol 96 pg 102006 (2017)\n"
"Written by Liam Dunn <liamd@student.unimelb.edu.au>\n"
"with contributions from Patrick Clearwater <p.clearwater@student.unimelb.edu.au>\n"
	, progname);
}

double fstat_read_block(FILE *obs_file, int block_size, double *freqs, int start_offset)
{
    printf("Start offset is %d\n", start_offset);
    char line[1000];
    int i = 0;
    int pos = 0;
    double freq_sum = 0;
    while(i < block_size)
    {
        pos++;
        char const * const rv = fgets(line, sizeof(line), obs_file);
        if(!rv) {
            fprintf(stderr, "Error reading fstat file, bailing out\n");
            exit(1);
        }
        // TODO: There is an edge case here where a line is over 1000
        // characters long; the code simply won't work. This usually won't
        // happen with a real F-stat file, but on the other hand someone could
        // put in a perversely long comment. Replace fgets() with getline()
        if(line[0] == '%' || pos < start_offset)
        {
            continue;
        }
        sscanf(line, "%lf %*f %*f %*f %*f %*f %*f", &freqs[i]);

        freq_sum += freqs[i];
        i++;
    }
    return freq_sum / block_size;
}

int main(int argc, char **argv)
{
    opterr = 0;
    static struct option long_options[] =
    {
        {"atoms", required_argument, 0, 0},
        {"central_a0", required_argument, 0, 0},
        {"a0_band", required_argument, 0, 0},
        {"a0_bins", required_argument, 0, 0},
        {"P", required_argument, 0, 0},
        {"central_P", required_argument, 0, 0},
        {"P_band", required_argument, 0, 0},
        {"P_bins", required_argument, 0, 0},
        {"central_phase", required_argument, 0, 0},
        {"phase_band", required_argument, 0, 0},
        {"phase_bins", required_argument, 0, 0},
        {"block_size", required_argument, 0, 0},
        {"fstat", required_argument, 0, 0},
        {"out_prefix", required_argument, 0, 0},
        {"obs_len", required_argument, 0, 0},
		{"help", no_argument, 0, 0},
		{"central_orbitTp", required_argument, 0, 0},
		{"orbitTp_band", required_argument, 0, 0},
		{"orbitTp_bins", required_argument, 0, 0},
		{"cjs_atoms", required_argument, 0, 0},
		{"start_time", required_argument, 0, 0},
		{"tblock", required_argument, 0, 0},
		{"use_callback", required_argument, 0, 0},
		{"ignore_wings", required_argument, 0, 0},
		{"print_ll", no_argument, 0, 0},
		{"threshold", required_argument, 0, 0},
		{"llthreshold", required_argument, 0, 0},
		{"dont_include_P_in_summary_file", required_argument, 0, 0},
		{"null_atoms", required_argument, 0, 0},
        {0,0,0,0}
    };

    char *atoms_dir = 0;
	char *cjs_atoms = 0;
	int start_time = 0;
	int tblock = 0;
    double central_a0 = NAN;
    double a0_band = 0;
    int a0_bins = 1;
    double central_P = NAN;
	double P_band = 0;
	int P_bins = 1;
    double central_phase = NAN;
    double phase_band = 0;
    int phase_bins = 1;
	double central_orbitTp = NAN;
	double orbitTp_band = 0;
    int block_size = 0;
    double central_freq;
    double bottom_freq;
    double top_freq;
    char *fstat_path = 0;
    const char *out_prefix = 0;
    int obs_len = 0;
    cudaError_t err;
	int help_flag = 0;
	int use_callback = 1;
	int ignore_wings = 0;
	int print_ll = 0;
	double score_threshold = DBL_MAX;
	double ll_threshold = DBL_MAX;
	int old_style_summary_file = -1;
	int period_set_with_P = 0;
	int null_atoms = -1;

    char c;
    while(1)
    {
        int option_index = 0;
        c = getopt_long(argc, argv, "", long_options, &option_index);
        if (c == -1)
            break;

        switch (c)
        {
        case 0:
            switch (option_index)
            {
            case 0:
                atoms_dir = (char *)malloc(strlen(optarg)+1);
                strcpy(atoms_dir, optarg);
                printf("atoms directory is %s\n", atoms_dir);
                break;
            case 1:
                sscanf(optarg, "%lf", &central_a0);
                printf("central a0 is %lf\n", central_a0);
                break;
            case 2:
                sscanf(optarg, "%lf", &a0_band);
                printf("a0 band is %lf\n", a0_band);
                break;
            case 3:
                sscanf(optarg, "%d", &a0_bins);
                printf("a0 bins is %d\n", a0_bins);
                break;
            case 4: /* --P -- same as central_P for backward compatibility */
				period_set_with_P = 1;
				/* fall through */
			case 5:
                sscanf(optarg, "%lf", &central_P);
                printf("central P is %lf\n", central_P);
                break;
			case 6:
				sscanf(optarg, "%lf", &P_band);
				printf("P band is %lf\n", P_band);
				break;
			case 7:
				sscanf(optarg, "%d", &P_bins);
				printf("P bins is %d\n", P_bins);
				break;
            case 8:
                sscanf(optarg, "%lf", &central_phase);
                printf("[!!!] Warning: searching over phase rather than orbitTp is not well-tested. Use at your own risk.\n");
                printf("Central phase is %lf\n", central_phase);
                break;
            case 9:
                sscanf(optarg, "%lf", &phase_band);
                printf("Phase band is %lf\n", phase_band);
                break;
            case 10:
                sscanf(optarg, "%d", &phase_bins);
                printf("Phase bins is %d\n", phase_bins);
                break;
            case 11:
                sscanf(optarg, "%d", &block_size);
                printf("Block size is %d\n", block_size);
                break;
            case 12:
                fstat_path = (char *)malloc(strlen(optarg)+1);
                strcpy(fstat_path, optarg);
                printf("Fstat path is %s\n", fstat_path);
                break;
            case 13: {
                char *tmp = (char *)malloc(strlen(optarg)+1);
                strcpy(tmp, optarg);
				out_prefix = tmp;
                printf("Out prefix is %s\n", out_prefix);
                break; }
            case 14:
                sscanf(optarg, "%d", &obs_len);
                printf("Custom obs len is %d\n", obs_len);
                break;
			case 15:
				help_flag = 1;
				break;
			case 16:
				sscanf(optarg, "%lf", &central_orbitTp);
				printf("orbitTp is %lf\n", central_orbitTp);
				break;
			case 17:
				sscanf(optarg, "%lf", &orbitTp_band);
				printf("orbitTp band is %lf\n", orbitTp_band);
				break;
			case 18:
				sscanf(optarg, "%d", &phase_bins);
				printf("phase_bins bins is %d (set through orbitTp handler)\n", phase_bins);
				break;
			case 19:
				cjs_atoms = (char *)malloc(strlen(optarg)+1);
				strcpy(cjs_atoms, optarg);
				printf("CJS atom pattern is %s\n", cjs_atoms);
				break;
			case 20:
				sscanf(optarg, "%d", &start_time);
				printf("Start time is %d\n", start_time);
				break;
			case 21:
				sscanf(optarg, "%d", &tblock);
				printf("tblock is %d\n", tblock);
				break;
			case 22:
				if(optarg[0] == '1' || optarg[0] == 'y' || optarg[0] == 'Y') {
					use_callback = 1;
					printf("Will use a CUDA callback\n");
				} else if(optarg[0] == '0' || optarg[0] == 'n' || optarg[0] == 'N') {
					use_callback = 0;
					printf("Will not use a CUDA callback\n");
				} else {
					printf("Didn't understand argument %s to --use_callback\n", optarg);
					exit(1);
				}
				break;
			case 23:
				sscanf(optarg, "%d", &ignore_wings);
				printf("ignore_wings is %d\n", tblock);
				break;
			case 24:
				print_ll = 1;
				printf("Found --print_ll, so will print loglikelihoods\n");
				break;
			case 25:
				sscanf(optarg, "%lf", &score_threshold);
				printf("score_threshold is %f\n", score_threshold);
				break;
			case 26:
				sscanf(optarg, "%lf", &ll_threshold);
				printf("ll_threshold is %f\n", ll_threshold);
				break;
			case 27:
				if(optarg[0] == '1' || optarg[0] == 'y' || optarg[0] == 'Y') {
					printf("Explicitly excluding P from summary file\n");
					old_style_summary_file = 1;
				} else if(optarg[0] == '0' || optarg[0] == 'n' || optarg[0] == 'N') {
					printf("Explicitly including P in summary file\n");
					old_style_summary_file = 0;
				} else {
					printf("Didn't understand argument %s to --dont_include_P_in_summary_file\n", optarg);
					exit(1);
				}
				break;
			case 28: // --null_atoms
				if(1 == sscanf(optarg, "%d", &null_atoms)) {
					printf("Expecting %d null atoms\n", null_atoms);
				} else {
					fprintf(stderr, "Problem parsing argument to --null_atoms\n");
					exit(1);
				}
				break;
            }
        }
    }

	if(help_flag) {
		usage(argv[0]);
		return 0;
	}

	// Check that we got all required arguments
	if(!atoms_dir && !cjs_atoms) {
		printf("Please specify directory containing Fstat atoms (--atoms or --cjs_atoms)\n");
		usage(argv[0]);
		return 1;
	} else if(atoms_dir && cjs_atoms) {
		printf("Please specify only one of --atoms or --cjs_atoms\n");
		usage(argv[0]);
		return 1;
	} else if(cjs_atoms) {
		if(!tblock) {
			printf("Please specify --tblock with --cjs_atoms\n");
			usage(argv[0]);
			return 1;
		}
		if(!start_time) {
			printf("Please specify --start_time with --cjs_atoms\n");
			usage(argv[0]);
			return 1;
		}
	}
	if(isnan(central_a0)) {
		printf("Please specify search a0 value (--central_a0)\n");
		usage(argv[0]);
		return 1;
	}
	if(isnan(central_P)) {
		printf("Please specify orbital period of the binary (--central_P)\n");
		usage(argv[0]);
		return 1;
	}
	if(isnan(central_phase) && isnan(central_orbitTp)) {
		printf("Please specify search phase (--central_phase or --central_orbitTp)\n");
		usage(argv[0]);
		return 1;
	} else if(!isnan(central_phase) && !isnan(central_orbitTp)) {
		printf("You can't specify both the phase and orbitTp.\n");
		return 1;
	} // Note: if central_orbitTp is specified, we need to convert that into phase -- but we do that later, because we need to know the start GPS time
	if(!block_size) {
		printf("Please specify the number of frequency bins (--block_size)\n");
		usage(argv[0]);
		return 1;
	}
	if(atoms_dir && !fstat_path) {
		printf("Please specify the path to an exemplar F-statistic output (--fstat) when using CFSv2 atoms\n");
		usage(argv[0]);
		return 1;
	}
	if(!out_prefix) {
		out_prefix = "gpusearch";
	}

	if(atoms_dir) {
		load_CFSv2_atoms_freqs(fstat_path, block_size, &top_freq, &central_freq, &bottom_freq);
	}

	// Figure out if we should use an "old-style" summary file (without the
	// orbital period printed). If the user has told us what to do, do that.
	// Otherwise, if the user used --P, assume they're used to the old
	// behaviour; if they specified --central_P, the new behaviour
	if(-1 == old_style_summary_file) {
		old_style_summary_file = period_set_with_P;
	} // else, specified as an argument

#ifdef SINGLE_PRECISION
	const char *prec = "single";
#else
	const char *prec = "double";
#endif
	printf("This version of the GPU code compiled for %s precision.\n", prec);

    cuDoubleComplex *fa_data_double;
    cuDoubleComplex *fb_data_double;
    vit_cuComplexType *fa_data;
    vit_cuComplexType *fb_data;
    vit_cuComplexType *fa_gpu_data;
    vit_cuComplexType *fb_gpu_data;
    vit_cuComplexType *fa_gpu_clean; // These two store the Fourier transformed Fa,
    vit_cuComplexType *fb_gpu_clean; // Fb, that are reused at each step

    cufftResult cufft_res;
    cudaError_t cuda_res;
	double *Ahats, *Bhats, *Chats, *Dhats;

	// Regarding obs_len and custom_obs_len -- we've cleverly arranged for
	// custom_obs_len to be inside obs_len. If it wasn't specified on the
	// command line, it'll be zero and load_CFSv2_atoms will set it for us.
    err = cudaSetDevice(0);
	if(cudaSuccess != err) {
		printf("Error in cudaSetDevice call: %s\n", cudaGetErrorString(cudaPeekAtLastError()));
		exit(1);
	}
	double *middle_times;
	if(atoms_dir) {
		load_CFSv2_atoms(atoms_dir, block_size, &obs_len, &fa_data_double, &fb_data_double, &Ahats, &Bhats, &Chats, &Dhats, &start_time, &tblock);
	} else {
		load_CJS_atoms(cjs_atoms, block_size, &obs_len, null_atoms, &fa_data_double, &fb_data_double, &Ahats, &Bhats, &Chats, &Dhats, &top_freq, &central_freq, &bottom_freq, &middle_times);
	}

#ifdef SINGLE_PRECISION
	// Convert the cuDoubleComplex that we loaded into cuFloatComplex
    cuda_checked_malloc_host((void**)&fa_data, sizeof(cuFloatComplex)*block_size*obs_len);
	cuda_checked_malloc_host((void**)&fb_data, sizeof(cuFloatComplex)*block_size*obs_len);
	for(size_t i = 0; i < (size_t)(block_size * obs_len); ++i) {
		fa_data[i] = (cuFloatComplex){ (float)cuCreal(fa_data_double[i]), (float)cuCimag(fa_data_double[i])};
		fb_data[i] = (cuFloatComplex){ (float)cuCreal(fb_data_double[i]), (float)cuCimag(fb_data_double[i])};
	}
#else
	// What we got out of load_CJS_atoms or load_CFSv2_atoms is fine, just alias the pointer
	fa_data = fa_data_double;
	fb_data = fb_data_double;
#endif
    
	const int trellis_block_size = block_size - 2*ignore_wings;
    double *jstat_gpu_data;
    vit_cuComplexType *filter_gpu_data;
    trellis_entry_t *trellis_gpu_data;
    double *middle_times_gpu_data;

    cuda_checked_malloc((void **)&fa_gpu_data, (sizeof(vit_cuComplexType)*block_size*obs_len));
    cuda_checked_malloc((void **)&fb_gpu_data, (sizeof(vit_cuComplexType)*block_size*obs_len));
    cuda_checked_malloc((void **)&fa_gpu_clean, (sizeof(vit_cuComplexType)*block_size*obs_len));
    cuda_checked_malloc((void **)&fb_gpu_clean, (sizeof(vit_cuComplexType)*block_size*obs_len));
    cuda_checked_malloc((void **)&jstat_gpu_data, (size_t)(block_size*obs_len*sizeof(double)));
    cuda_checked_malloc((void **)&filter_gpu_data, (size_t)(block_size*obs_len*sizeof(vit_cuComplexType)));
    cuda_checked_malloc((void **)&trellis_gpu_data, (size_t)(block_size*obs_len*sizeof(trellis_entry_t)));
    cuda_checked_malloc((void **)&middle_times_gpu_data, (size_t)(obs_len*sizeof(double)));
    


    // Create FFT plan for Fa and Fb
    cufftHandle fstat_plan;
    if(cufftPlan1d(&fstat_plan, block_size, vit_CUFFT_X2X, 1) != CUFFT_SUCCESS) {
        printf("fstat_plan creation failed!\n");
		exit(1);
	}

	// Compute the FFTs
    for(int i = 0; i < obs_len; i++) {
        // Compute the FFTs of Fa and Fb and store them in CPU memory to be reloaded for each parameter choice
        cuda_res = cudaMemcpy((void *)(fa_gpu_data + i*block_size), (void *)(fa_data + i*block_size), block_size*sizeof(vit_cuComplexType), cudaMemcpyHostToDevice);
        cuda_res = cudaMemcpy((void *)(fb_gpu_data + i*block_size), (void *)(fb_data + i*block_size), block_size*sizeof(vit_cuComplexType), cudaMemcpyHostToDevice);
        if((cufft_res = vit_cufftExecX2X(fstat_plan, fa_gpu_data + i*block_size, fa_gpu_clean + i*block_size, CUFFT_FORWARD)) != CUFFT_SUCCESS) {
            printf("FFT execution failed! res = %d\n", cufft_res);
			exit(1);
		}
        if((cufft_res = vit_cufftExecX2X(fstat_plan, fb_gpu_data + i*block_size, fb_gpu_clean + i*block_size, CUFFT_FORWARD)) != CUFFT_SUCCESS) {
            printf("FFT execution failed! res = %d\n", cufft_res);
			exit(1);
		}
	}

    
	FILE *a0_phase_loglikes_scores = checked_fopen("w", "%s_a0_phase_loglikes_scores.dat", out_prefix);
	FILE *scores_above_threshold_fh = 0;
	FILE *ll_above_llthreshold_fh = 0;
    double max_loglike = -INFINITY;
    double max_a0 = -INFINITY;
    double max_phase = -INFINITY;
    double max_score = -INFINITY;
    double max_freq = -INFINITY;
    double mean_score = 0;
    trellis_entry_t *best_trellis = (trellis_entry_t *)malloc(sizeof(trellis_entry_t)*block_size);
    int *best_path = (int *)malloc(sizeof(int)*obs_len);
    double bins_per_hz = block_size/(top_freq - bottom_freq);
    cufftHandle filter_plan;
    int embed[] = {block_size};
    if((cufft_res = cufftPlanMany(&filter_plan, 1, &block_size, embed, 1, block_size, embed, 1, block_size, vit_CUFFT_X2X, obs_len)) != CUFFT_SUCCESS)
    {
        printf("Filter plan creation failed!%d\n", cufft_res);
        exit(1);
    }

    cufftHandle fstat_inv_plan;

	if(cufftPlanMany(&fstat_inv_plan, 1, &block_size, embed, 1, block_size, embed, 1, block_size, vit_CUFFT_X2X, obs_len) != CUFFT_SUCCESS) {
		printf("fstat_inv_plan creation failed!\n");
		exit(1);
	}

	if(use_callback) {

		conv_cb_params host_params = (conv_cb_params) { filter_gpu_data, block_size };
		conv_cb_params *device_params;
		err = cudaMalloc((void **)&device_params, sizeof(conv_cb_params));
		cuda_res = cudaMemcpy(device_params, &host_params, sizeof(conv_cb_params), cudaMemcpyHostToDevice);
		if(cudaSuccess != cuda_res) {
			printf("Error doing a host -> device memcpy, error: %s\n", cudaGetErrorString(cudaPeekAtLastError()));
			exit(1);
		}

		vit_cufftCallbackLoadXXX host_callback_ptr;
		cuda_res = cudaMemcpyFromSymbol(&host_callback_ptr, convolve_cb_ptr, sizeof(host_callback_ptr));
		cufft_res = cufftXtSetCallback(fstat_inv_plan, (void **)&host_callback_ptr, vit_CUFFT_CB_LOAD_TYPE, (void **)&device_params);
        if(cufft_res != CUFFT_SUCCESS)
        {
            printf("Callback setup failed!\n");
            exit(1);
            }

	}


	int use_orbitTp = isnan(central_phase);
	for(int P_bin = 0; P_bin <  P_bins; P_bin++) {
		const double P = central_P + (P_bins > 1 ? -(P_band/2) + ((double)P_bin/(P_bins-1))*P_band : 0);
		// If the user wants to search orbitTp rather than phase, we need to
		// convert orbitTp values into phase values. (We couldn't do this until we
		// loaded the start GPS times, above.)
		if(use_orbitTp) {
			double dummy;
			central_phase = 2 * M_PI * modf(((start_time - central_orbitTp) / P) - 0.149272, &dummy );
			if(central_phase < 0) central_phase += 2 * M_PI;
			phase_band = 2 * M_PI * (orbitTp_band / P);
			// No separate variable for orbitTp_bins
			cudaMemcpy(middle_times_gpu_data, middle_times, sizeof(double)*obs_len, cudaMemcpyHostToDevice);
		}

    for(int a0_bin = 0; a0_bin < a0_bins; a0_bin++)
    {
        double a0;
        if(a0_bins > 1)
            a0 = central_a0 - (a0_band/2) + ((double)a0_bin/(a0_bins-1))*a0_band;
        else
            a0 = central_a0;
        // M is the number of non-negligible sidebands which need to be incorporated into the Jstat
        const int M = (int)2*ceil(2*M_PI*central_freq*a0)+1;
        const int highest_sideband_idx = (int)ceil(2*M_PI*central_freq*a0);

        for(int phase_bin = 0; phase_bin < phase_bins; phase_bin++)
        {
            clock_t start = clock();
            // Load the clean Fa and Fb FFT data
            err = cudaMemcpyAsync(fa_gpu_data, fa_gpu_clean, block_size*obs_len*sizeof(vit_cuComplexType), cudaMemcpyDeviceToDevice);
            err = cudaMemcpyAsync(fb_gpu_data, fb_gpu_clean, block_size*obs_len*sizeof(vit_cuComplexType), cudaMemcpyDeviceToDevice);
            if(a0_bin % 100 == 0)
                printf("a0 = %.16lf phase bin = %d\n", a0, phase_bin);

            cudaMemset(filter_gpu_data, 0, block_size*obs_len*sizeof(vit_cuComplexType));
            if(use_orbitTp)
            {
                double orbitTp;
                if(phase_bins > 1)
                    orbitTp = central_orbitTp - (orbitTp_band / 2) + ( (double)phase_bin / (phase_bins - 1) )*orbitTp_band;
                else
                    orbitTp = central_orbitTp;

                compute_filter_atomic_orbitTp<<<((highest_sideband_idx+1)*obs_len + 255)/256, 256>>>(filter_gpu_data, bottom_freq, top_freq, central_freq, a0, middle_times_gpu_data, orbitTp, P, highest_sideband_idx, block_size, obs_len, bins_per_hz);
            }
            else
            {
                double starting_phase;
                if(phase_bins > 1)
                    starting_phase = central_phase - (phase_band/2) + ((double)phase_bin/(phase_bins-1))*phase_band;
                else
                    starting_phase = central_phase;
                compute_filter_atomic_phase<<<((M-1)/2*obs_len + 255)/256, 256>>>(filter_gpu_data, bottom_freq, top_freq, central_freq, a0, tblock, starting_phase, P, highest_sideband_idx, block_size, obs_len, bins_per_hz);
                cuda_res = cudaPeekAtLastError();
            }

            cufft_res = vit_cufftExecX2X(filter_plan, filter_gpu_data, filter_gpu_data, CUFFT_FORWARD);
            if(cufft_res != CUFFT_SUCCESS) {
                printf("Execution failed (ExecC2C: filter)! %s\n", cudaGetErrorString(cudaPeekAtLastError()));
                printf("ptr = %lu\n", (unsigned long)filter_gpu_data);
                printf("block_size = %lu\n", (unsigned long)block_size);
                printf("cufft result = %lu\n", (unsigned long)cufft_res);
                exit(1);
            }

            if(!use_callback) {
                // Pointwise multiply the Fa and Fb FFTs by the filter FFT
				// We only do this in the non-callback case -- in the callback
				// case, this gets done automatically by the callback. Note
				// that because each step has a different filter (different
				// phase term), the filters are contiguous in memory and the
				// 'convolution' is pointwise multiplication, we can do this in
				// a single kernel invocation
				jstat_convolve<<<(obs_len*block_size+255)/256, 256>>>(fa_gpu_data, fb_gpu_data, filter_gpu_data, block_size, obs_len * block_size);
            }

			if(vit_cufftExecX2X(fstat_inv_plan, fa_gpu_data, fa_gpu_data, CUFFT_INVERSE) != CUFFT_SUCCESS) {
				printf("Execution failed (callbacks, ExecC2C: fa)! error: %s\n", cudaGetErrorString(cudaPeekAtLastError()));
				exit(1);
			}
			if(vit_cufftExecX2X(fstat_inv_plan, fb_gpu_data, fb_gpu_data, CUFFT_INVERSE) != CUFFT_SUCCESS) {
				printf("Execution failed (callbacks, ExecC2C: fb)! error: %s\n", cudaGetErrorString(cudaPeekAtLastError()));
				exit(1);
			}

            for(int i = 0; i < obs_len; i++)
            {
                jstat<<<(block_size+255)/256, 256>>>(fa_gpu_data + i*block_size, fb_gpu_data + i*block_size, jstat_gpu_data + i*block_size, Ahats[i], Bhats[i], Chats[i], Dhats[i], i, block_size);
            }

            viterbi_run(jstat_gpu_data, trellis_gpu_data, block_size, trellis_block_size, ignore_wings, obs_len);
            int *path = (int *)malloc(sizeof(int)*obs_len);

            double score;
			int *paths_above_threshold = 0; // the actual above-threshold paths (which we ignore)
			int count_paths_above_threshold = 0; // *number* of paths above the threshold
			int count_paths_above_llthreshold = 0; // *number* of paths above the ll threshold
			double *ll_above_llthreshold = 0; // and the LLs themselves
			double *scores_above_threshold = 0;
            double loglike = viterbi_results(trellis_gpu_data, trellis_block_size, obs_len, max_score, path, NULL, 0, &paths_above_threshold, &scores_above_threshold, score_threshold, &count_paths_above_threshold, &score, &count_paths_above_llthreshold, &ll_above_llthreshold, ll_threshold, best_trellis);
            mean_score += score;
			double phase_like_thing; // either phi or orbitTp (used for display only)
            if(use_orbitTp)
            {
                if(phase_bins > 1)
                    phase_like_thing = central_orbitTp - (orbitTp_band / 2) + ( (double)phase_bin / (phase_bins - 1) )*orbitTp_band;
                else
                    phase_like_thing = central_orbitTp;
            }
            else
            {
                if(phase_bins > 1)
                    phase_like_thing = central_phase - (phase_band/2) + ((double)phase_bin/(phase_bins-1))*phase_band;
                else
                    phase_like_thing = central_phase;
            }
			if(old_style_summary_file) {
				fprintf(a0_phase_loglikes_scores, "%.16lf %.16lf %.16lf %.16lf %.16lf\n", a0, phase_like_thing, loglike, score, bottom_freq + (path[obs_len-1]+ignore_wings) * ((top_freq - bottom_freq) / block_size));
			} else {
				fprintf(a0_phase_loglikes_scores, "%.16f %.16lf %.16lf %.16lf %.16lf %.16lf\n", P, a0, phase_like_thing, loglike, score, bottom_freq + (path[obs_len-1]+ignore_wings) * ((top_freq - bottom_freq) / block_size));
			}
			// Save any scores that are above the threshold
			if(count_paths_above_threshold) {
				if(!scores_above_threshold_fh) {
					scores_above_threshold_fh = checked_fopen("w", "%s_above_threshold.dat", out_prefix);
				}
				for(size_t i = 0; i < (size_t)count_paths_above_threshold; ++i) {
					fprintf(scores_above_threshold_fh, "%0.16lf\n", scores_above_threshold[i]);
				}
			}
			free(scores_above_threshold);
			free(paths_above_threshold);
			if(count_paths_above_llthreshold) {
				if(!ll_above_llthreshold_fh) {
					ll_above_llthreshold_fh = checked_fopen("w", "%s_above_llthreshold.dat", out_prefix);
				}
				for(size_t i = 0; i < (size_t)count_paths_above_llthreshold; ++i) {
					fprintf(ll_above_llthreshold_fh, "%0.16lf\n", ll_above_llthreshold[i]);
				}
			}
			free(ll_above_llthreshold);

            // Keep track of the best scoring parameter choice
            if(score > max_score)
            {
                memcpy(best_path, path, sizeof(int)*obs_len);
                max_score = score;
                max_loglike = loglike;
                max_a0 = a0;
				max_phase = phase_like_thing;
                max_freq = bottom_freq + (double)(path[obs_len-1] + ignore_wings) * ((top_freq - bottom_freq)/block_size);
                printf("NEW MAX: Score = %lf, loglike = %lf, a0 = %.16lf, phase = %.16lf, freq = %.16lf, P = %.16lf\n", max_score, max_loglike, max_a0, max_phase, max_freq, P);
            }
            free(path);
            clock_t diff = clock() - start;
            //int msec = diff * 1000 / CLOCKS_PER_SEC;
            //printf("Time taken %d seconds %d milliseconds\n", msec/1000, msec%1000);
        }
    }
	}
    fclose(a0_phase_loglikes_scores);
	if(scores_above_threshold_fh) fclose(scores_above_threshold_fh);
    printf("Mean score: %.16lf\n", mean_score/(a0_bins*phase_bins));

    FILE *path_file = checked_fopen("w", "%s_path.dat", out_prefix);
    for(int i = 0; i < obs_len; i++)
    {
		const double frequency = bottom_freq + (best_path[i] + ignore_wings) * ((top_freq - bottom_freq) / block_size);
        fprintf(path_file, "%.16lf\n", frequency);
    }
    fclose(path_file);

    FILE *scores_file = checked_fopen("w", "%s_scores.dat", out_prefix);
	FILE *ll_file = 0;
	if(print_ll) {
		ll_file = checked_fopen("w", "%s_ll.dat", out_prefix);
	}
    double loglike_mean = 0;
    double loglike_variance = 0;
    for(int i = 0; i < trellis_block_size; i++)
    {
        loglike_mean += best_trellis[i].loglike;
    }
	loglike_mean /= trellis_block_size;
    for(int i = 0; i < trellis_block_size; i++)
    {
        loglike_variance += pow(best_trellis[i].loglike - loglike_mean, 2)/trellis_block_size;
    }
    double loglike_stddev = sqrt(loglike_variance);
    for(int i = 0; i < trellis_block_size; i++)
    {
        fprintf(scores_file, "%.16lf\n", (best_trellis[i].loglike - loglike_mean)/loglike_stddev);
		if(print_ll) {
			fprintf(ll_file, "%.16lf\n", best_trellis[i].loglike);
		}
    }
    fclose(scores_file);
	if(print_ll) {
		fclose(ll_file);
	}
    printf("Calculated variance: %.16lf\n", loglike_variance);
    printf("Calculated mean: %.16lf\n", loglike_mean);
    printf("Max loglike = %lf, max score = %lf --- a0 = %.16lf, phase = %.16lf, freq = %.16lf\n", max_loglike, max_score, max_a0, max_phase, max_freq);
}

// Exactly the same as cudaMalloc, except if an error occurs, print it and
// exit.
void cuda_checked_malloc(void **const ptr, const size_t sz) {
	cudaError_t err = cudaMalloc(ptr, sz);
	if(cudaSuccess != err) {
		printf("Error allocating memory: %s\n", cudaGetErrorString(cudaPeekAtLastError()));
		exit(1);
	}
}

void cuda_checked_malloc_host(void **const ptr, const size_t sz) {
	cudaError_t err = cudaMallocHost(ptr, sz);
	if(cudaSuccess != err) {
		printf("Error allocating memory: %s\n", cudaGetErrorString(cudaPeekAtLastError()));
		exit(1);
	}
}

// Load atoms that are in the "CFSv2" format (i.e., the original legacy format,
// which was first that the GPU code supported and initally used when we first
// developed the algorithm)

// Load the frequencies from the exemplar F-stat file
void load_CFSv2_atoms_freqs(const char *const fstat_path, const int block_size, double *const top_freq, double *const central_freq, double *const bottom_freq) {
    // We read a single Fstat file to obtain the frequency band we're operating in
    FILE *fstat_file = checked_fopen("r", "%s", fstat_path);
    double *freqs = (double *)malloc(sizeof(double)*block_size);
    fstat_read_block(fstat_file, block_size, freqs, 0);
    *bottom_freq = freqs[0];
    *top_freq = freqs[block_size-1];
    *central_freq = 0;
    for(int i = 0; i < block_size; i++)
    {
        *central_freq += freqs[i]/block_size;
    }
    printf("Bottom freq: %.16lf\nCentral freq: %.16lf\nTop freq: %.16lf\n", *bottom_freq, *central_freq, *top_freq);
	free(freqs);

}

void load_CFSv2_atoms(
	const char *const atoms_dir,
	const int block_size,
	int *const out_obs_len,
	cuDoubleComplex **const fa_data_ptr,
	cuDoubleComplex **const fb_data_ptr,
	double **const Ahats_ptr,
	double **const Bhats_ptr,
	double **const Chats_ptr,
	double **const Dhats_ptr,
	int *const startGPStime,
	int *const tblock
) {
    char *fa_glob_pattern = (char *)malloc(strlen(atoms_dir)+100);
    sprintf(fa_glob_pattern, "%s/effatoms-*-L1_Fa.bin", atoms_dir);
    glob_t fa_glob;
    glob(fa_glob_pattern, GLOB_ERR, NULL, &fa_glob);

    char *fb_glob_pattern = (char *)malloc(strlen(atoms_dir)+100);
    sprintf(fb_glob_pattern, "%s/effatoms-*-L1_Fb.bin", atoms_dir);
    glob_t fb_glob;
    glob(fb_glob_pattern, GLOB_ERR, NULL, &fb_glob);


    char *meta_glob_pattern = (char *)malloc(strlen(atoms_dir)+100);
    sprintf(meta_glob_pattern, "%s/effatoms-*-L1_meta.bin", atoms_dir);
    glob_t meta_glob;
    glob(meta_glob_pattern, GLOB_ERR, NULL, &meta_glob);

    // In the absence of a user-provided obs_len, assume that obs_len is the number of meta files in the directory
    int obs_len = meta_glob.gl_pathc;
    if(*out_obs_len) {
        obs_len = *out_obs_len;
	} else {
		*out_obs_len = obs_len;
	}

	cudaError_t err;
	// Now allocate memory for fa_data and fb_data
    cuda_checked_malloc_host((void**)fa_data_ptr, sizeof(cuDoubleComplex)*block_size*obs_len);
	cuda_checked_malloc_host((void**)fb_data_ptr, sizeof(cuDoubleComplex)*block_size*obs_len);
	// These are just convenience pointers
	cuDoubleComplex *const fa_data = *fa_data_ptr;
	cuDoubleComplex *const fb_data = *fb_data_ptr;
	(void)err;

	// These get used for processing but free()ed at the end
    struct meta *meta_data = (struct meta *)malloc(2*sizeof(struct meta)*480);
    double *Ahats = (double *)malloc(sizeof(double)*obs_len);
    double *Bhats = (double *)malloc(sizeof(double)*obs_len);
    double *Chats = (double *)malloc(sizeof(double)*obs_len);
    double *Dhats = (double *)malloc(sizeof(double)*obs_len);
	*Ahats_ptr = Ahats;
	*Bhats_ptr = Bhats;
	*Chats_ptr = Chats;
	*Dhats_ptr = Dhats;
    unsigned int *GPStimes = (unsigned int *)malloc(sizeof(unsigned int)*obs_len);

    for(int i = 0; i < obs_len; i++)
    {
        char file_name[255];
        char *dir = dirname(fa_glob.gl_pathv[i]);
        sprintf(file_name, "%s/effatoms-%d-L1_Fa.bin", dir, i);
        printf("%s\n", file_name);
        FILE *fa_file = fopen(file_name,"r");
        sprintf(file_name, "%s/effatoms-%d-L1_Fb.bin", dir, i);
        printf("%s\n", file_name);
        FILE *fb_file = fopen(file_name,"r");
        sprintf(file_name, "%s/effatoms-%d-L1_meta.bin", dir, i);
        printf("%s\n", file_name);
        FILE *meta_file = fopen(file_name,"r");

        // The meta files consist of 2 ifos*480 SFTs meta structs
        printf("Reading data from disk...");
        if(2*480 != fread(meta_data, sizeof(struct meta),  2*480, meta_file)) {
            fprintf(stderr, "Didn't read the correct number of meta structs from the CFS_v2 atoms (expected 480, you'll need to adjust the code if that expectation is no longer correct)\n");
            exit(1);
        }

        Ahats[i] = 0;
        Bhats[i] = 0;
        Chats[i] = 0;
        GPStimes[i] = meta_data[0].time;
        printf("%d\n%d\n", meta_data[0].time, meta_data[1].time);
        for(int j = 0; j < 2*480; j++)
        {
            Ahats[i] += meta_data[j].asq;
            Bhats[i] += meta_data[j].bsq;
            Chats[i] += meta_data[j].ab;
        }
        Dhats[i] = Ahats[i]*Bhats[i] - Chats[i]*Chats[i];
        for(int j = 0; j < block_size; j++)
        {
            int read = fread(&fa_data[j + i*block_size], sizeof(cuDoubleComplex), 1, fa_file);
            read += fread(&fb_data[j + i*block_size], sizeof(cuDoubleComplex), 1, fb_file);
            cuDoubleComplex temp = {0., 0.};
            read += fread(&temp, sizeof(cuDoubleComplex), 1, fa_file);
            fa_data[j + i*block_size] = cuCadd(fa_data[j + i*block_size], temp);
            read += fread(&temp, sizeof(cuDoubleComplex), 1, fb_file);
            fb_data[j + i*block_size] = cuCadd(fb_data[j + i*block_size], temp);
            if(2 != read) {
                fprintf(stderr, "Couldn't read datapoint %d from CFS_v2 atoms, bailing out\n", j);
                exit(1);
            }
        }
        printf("done.\n");

        fclose(fa_file);
        fclose(fb_file);
        fclose(meta_file);
    }

	*startGPStime = GPStimes[0];
	// TODO: should we check obs_len > 1?
	*tblock = GPStimes[1] - GPStimes[0];


	free(meta_data);
	free(GPStimes);
}

struct __attribute__((__packed__)) FstatAtom {
	double freq;
	float _Complex fa;
	float _Complex fb;
};

struct __attribute__((__packed__)) AntennaPatternMatrix {
	float A;
	float B;
	float C;
	float E;
	float D;
	char padding[4];
	double SinvT;
};

void load_CJS_atoms(
	const char *cjs_pattern,
	const int block_size,
	int *const obslen,
	const int null_atom_count,
	cuDoubleComplex **const fa_data_ptr,
	cuDoubleComplex **const fb_data_ptr,
	double **const Ahats_ptr,
	double **const Bhats_ptr,
	double **const Chats_ptr,
	double **const Dhats_ptr,
	double *top_freq,
	double *central_freq,
	double *bottom_freq,
	double **middle_times
) {
	// We find our files by substituting 0, 1, etc into cjs_pattern. If the
	// user has specified *obs_len, we use those numbers; otherwise, we find
	// the highest-numbered pattern that exists
	char *filename = (char *)malloc(strlen(cjs_pattern) + 9 + 1);
	if(!*obslen) {
		int expected_null_atoms = null_atom_count;
		while(1) {
			sprintf(filename, cjs_pattern, *obslen);
			// Test the file with fopen(2) rather than stat(2) -- will test
			// permissions etc
			FILE *fh = fopen(filename, "r");
			if(!fh) {
				--expected_null_atoms;
				if(expected_null_atoms < 0) {
					// The < test:
					// if --null_atoms is not set, e_n_a = -1 and so we fall
					// out of this loop as soon as it's hit
					// if --null_atoms is set, then we "count down" how many
					// more null atoms we're allowed to see. However, '0' is
					// OK: that just means we don't allow any more, but we do
					// allow this one.
					break;
				}
			}
			fclose(fh);
			++(*obslen);
		}
	}

	// Now obslen is correct
	cuda_checked_malloc_host((void**)fa_data_ptr, sizeof(cuDoubleComplex)*block_size*(*obslen));
	cuda_checked_malloc_host((void**)fb_data_ptr, sizeof(cuDoubleComplex)*block_size*(*obslen));
	cuDoubleComplex *const fa_data = *fa_data_ptr;
	cuDoubleComplex *const fb_data = *fb_data_ptr;
	*middle_times = (double *)malloc(sizeof(double)*(*obslen));
	double *Ahats = (double *)malloc(sizeof(double)*(*obslen));
	double *Bhats = (double *)malloc(sizeof(double)*(*obslen));
	double *Chats = (double *)malloc(sizeof(double)*(*obslen));
	double *Dhats = (double *)malloc(sizeof(double)*(*obslen));
	*Ahats_ptr = Ahats;
	*Bhats_ptr = Bhats;
	*Chats_ptr = Chats;
	*Dhats_ptr = Dhats;

	int expected_null_atoms = null_atom_count;
	for(int i = 0; i < *obslen; ++i) {
		const size_t array_ofs = i * block_size;
		sprintf(filename, cjs_pattern, i);
		FILE *fh = fopen(filename, "r");
		if(!fh) {
			--expected_null_atoms;
			if(expected_null_atoms < 0) { // see above for explanation of "< 0"
				fprintf(stderr, "Error opening file %s, errno = %d\n", filename, errno);
				exit(1);
			}

			// Make 'null' atoms, i.e., atoms with constant Fa, Fb
			for(size_t j = 0; j < (size_t)block_size; ++j) {
				fa_data[array_ofs + j] = make_cuDoubleComplex(1, 1);
				fb_data[array_ofs + j] = make_cuDoubleComplex(1, 1);
				(*middle_times)[i] = (*middle_times)[i-1];
			}
			Ahats[i] = 1;
			Bhats[i] = 1;
			Chats[i] = 1;
			Dhats[i] = 1;
		} else {
			// Load from *fh

			// Version
			char version;
			if(1 != fread(&version, sizeof(char), 1, fh)) {
				fprintf(stderr, "Bad read (when reading version) from %s\n", filename);
				exit(1);
			}
			if(version != 2) {
				fprintf(stderr, "CJS atom file %s is the wrong version (expected 2, got %d)\n", filename, (int)version);
				exit(1);
			}

			int gps_start, gps_end;
			if(1 != fread(&gps_start, sizeof(int), 1, fh)) {
				fprintf(stderr, "Bad read (when GPS start time) from %s\n", filename);
				exit(1);
			}
			if(1 != fread(&gps_end, sizeof(int), 1, fh)) {
				fprintf(stderr, "Bad read (when GPS end time) from %s\n", filename);
				exit(1);
			}
			(*middle_times)[i] = gps_start + (gps_end - gps_start)/2.;

			// Length + data
			// TODO: should we make block_size optional (as a command line arg)?
			size_t length;
			if(1 != fread(&length, sizeof(size_t), 1, fh)) {
				fprintf(stderr, "Bad read (when reading length) from %s\n", filename);
				exit(1);
			}
			if(length != (size_t)block_size) {
				fprintf(stderr, "CJS atom file %s has the wrong number of data values (expected %lu, got %lu)\n", filename, (size_t)block_size, length);
				exit(1);
			}
			double central_freq_ax = 0;
			for(size_t j = 0; j < length; ++j) {
				FstatAtom elem;
				// TODO: should we read these in blocks for massive speedups? or
				// mmap()?
				if(1 != fread(&elem, sizeof(FstatAtom), 1, fh)) {
					fprintf(stderr, "Bad read (when reading data element %lu of %lu) from %s\n", j, length, filename);
					exit(1);
				}
				fa_data[array_ofs + j] = make_cuDoubleComplex(crealf(elem.fa), cimagf(elem.fa));
				fb_data[array_ofs + j] = make_cuDoubleComplex(crealf(elem.fb), cimagf(elem.fb));
				if(0 == j) *bottom_freq = elem.freq;
				else if(length - 1 == j) *top_freq = elem.freq;
				central_freq_ax += elem.freq / block_size; // Same algorithm as in load_CFS_v2_atoms
			}
			*central_freq = central_freq_ax;
			
			// Antenna pattern function
			AntennaPatternMatrix apm;
			if(1 != fread(&apm, sizeof(AntennaPatternMatrix), 1, fh)) {
				fprintf(stderr, "Bad read (when readng antenna pattern data) from %s\n", filename);
				exit(1);
			}
			Ahats[i] = apm.A;
			Bhats[i] = apm.B;
			Chats[i] = apm.C;
			Dhats[i] = apm.D;
		}
	}

	if(expected_null_atoms > 0) {
		// if = 0, we're fine, so no need to display the error message
		// if = -1, expected_null_atoms was not set. If some null atoms were
		// found we would have exited already; since they weren't, we expected
		// none and found none so we're fine
		fprintf(stderr, "ERROR - I found fewer null atoms than I expected to\n");
		fprintf(stderr, "        Specifically, expected %d but found only %d\n", null_atom_count, null_atom_count - expected_null_atoms);
		exit(1);
	}
}

// fopen the requested file, dying if it can't be opened
// The first argument is the mode, passed straight to fopen()
// The second argument is a sprintf-type format string, and all remaining
// arguments are the arguments to that string
FILE *checked_fopen(const char *const mode, const char *const format_str, ...) {
	va_list args;
	va_start(args, format_str);
	int size = vsnprintf(0, 0, format_str, args);
	va_end(args);

	if(size < 0) {
		fprintf(stderr, "Error preparing the filename of one of the output files (?!?)\n");
		exit(1);
	}

	++size;
	char *filename = (char *)malloc(size);

	va_start(args, format_str);
	vsnprintf(filename, size, format_str, args);
	va_end(args);

	FILE *rv = fopen(filename, mode);
	if(!rv) {
		fprintf(stderr, "Error opening '%s'; errno = %d\n", filename, errno);
		exit(1);
	}

	free(filename);
	return rv;
}
