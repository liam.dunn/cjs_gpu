#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long;

sub main {
	my ($outfile, $length, $frequencybase, $starttime);
	Getopt::Long::GetOptions(
		'outfile=s'       => \$outfile,
		'length=i'        => \$length,
		'frequencybase=s' => \$frequencybase,
		'starttime=s'     => \$starttime,
	);
	$length //= 2097152;
	my @frequencies = read_frequencybase($frequencybase);
	print "frequencies read\n";
	defined $starttime or die "starttime";

	defined $outfile or die "specify outfile";
	open my $fh, '>', $outfile or die "Couldn't open output file $outfile";
	print $fh pack('c', 2); # version
	print $fh pack('ll', $starttime, $starttime + 864000); # start, end time
	print $fh pack('Q', $length); # data length
	for (1..$length) {
		print $fh pack('dffff', $frequencies[$_-1], 1, 1, 1, 1);
	}
	# antenna pattern matrix
	print $fh pack('ffffffd', 1, 1, 1, 1, 1, 0, 1);
	close $fh;
}

sub read_frequencybase {
	my ($f) = @_;
	my @rv;
	my $buf;
	open my $fh, '<', $f or die "Couldn't open base frequency file $f";
	read $fh, $buf, 1+8+4+4;
	my ($ver, undef, undef, $len) = unpack('cllQ', $buf);
	print "length = $len\n";
	$ver == 2 or die "Bad version";
	foreach (1..$len) {
		read $fh, $buf, 8+8+8;
		my ($freq, undef, undef, undef, undef) = unpack('dffff', $buf);
		push @rv, $freq;
	}
	close $fh;
	return @rv;
}

main;
