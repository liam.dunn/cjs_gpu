#!/usr/bin/bash

set -o errexit
set -o pipefail
set -o nounset

O2_START=1164556817

make_data() {
	# OK, we need to make some data with gaps
	# We'll generate 10 days, like this:
	#  1234567890
	#  XYX_YY__XX
	# where the 'X's are full day and the Ys are at 50% duty cycle
	mkdir -p fakedata/

	i=0
	for day in X Y X _ Y Y _ _ X X ; do
		starttime=$(( O2_START + 86400*$i))
		if [ $day == "X" ] ; then
			for ifo in H1 L1 ; do
				lalapps_Makefakedata_v4 \
					--outSingleSFT=1 \
					--outSFTbname=$(printf fakedata/day-%02d-%s.sft $i $ifo ) \
					--IFO=$ifo \
					--startTime=$starttime \
					--duration=86400 \
					--fmin=110.0 \
					--Band=3 \
					--noiseSqrtSh=2e-24
			done
		elif [ $day == "Y" ] ; then
			for ifo in H1 L1 ; do
				lalapps_Makefakedata_v4 \
					--outSingleSFT=1 \
					--outSFTbname=$(printf fakedata/day-%02d-%s.sft $i $ifo ) \
					--IFO=$ifo \
					--startTime=$starttime \
					--duration=$(( 86400 / 2 )) \
					--fmin=110.0 \
					--Band=3 \
					--noiseSqrtSh=2e-24
			done
		elif [ $day == "_" ] ; then
			# do nothing
			true
		else
			echo "??"
		fi

		i=$(( $i + 1 ))
	done
}

run_cjs() {
	mkdir -p atoms
	lalapps_ComputeJStatistic \
		--alpha=4.27569923849971 \
		--delta=-0.250624917263256 \
		--minStartTime=$O2_START \
		--maxStartTime=$(( $O2_START + 86400*10 )) \
		--freqStart=110.5 \
		--freqBand=2.0 \
		--bandWingSize=0 \
		--dFreq=5.787037037037037e-06 \
		--dataFiles=fakedata/*.sft \
		--asini 1 \
		--orbital-P 1 \
		--phi 1 \
		--driftTime 86400 \
		--saveFstatAtoms atoms/the-%02d.atoms

	# Version with 'handmade' null atoms for comparison
	cp -r atoms atoms_withnull
	for i in 3 6 7 ; do
		perl make-null-jatoms-v2.pl \
			--outfile atoms_withnull/the-0${i}.atoms \
			--length 345600 \
			--frequencybase atoms_withnull/the-00.atoms \
			--starttime $(( $O2_START + 86400*$i ))
	done
}

test_sameasnull() {
	TO_TEST=$1
	../bin/$TO_TEST \
		--cjs_atoms=atoms_withnull/the-%02d.atoms \
		--start_time=$O2_START \
		--tblock 86400 \
		--central_a0=1.44 \
		--central_P=68023.7 \
		--central_orbitTp=$O2_START \
		--block_size 345600 \
		--ignore_wings 41728 \
		--out_prefix cjs_gpu_out/outnull \
		--obs_len 10 >/dev/null

	../bin/$TO_TEST \
		--cjs_atoms=atoms/the-%02d.atoms \
		--start_time=$O2_START \
		--tblock 86400 \
		--central_a0=1.44 \
		--central_P=68023.7 \
		--central_orbitTp=$O2_START \
		--block_size 345600 \
		--ignore_wings 41728 \
		--out_prefix cjs_gpu_out/outignored \
		--obs_len 10 \
		--null_atoms 3 >/dev/null

	if diff -q cjs_gpu_out/outignored_scores.dat cjs_gpu_out/outnull_scores.dat ; then
		RESULT="passed"
	else
		RESULT="FAILED"
	fi
	printf "Test (%27s): %-70s: %s\n" "$TO_TEST" "produces same answer as 'null' atoms" "$RESULT"
}

test_simpleerrors() {
	TO_TEST=$1

	if ../bin/$TO_TEST \
		--cjs_atoms=atoms/the-%02d.atoms \
		--start_time=$O2_START \
		--tblock 86400 \
		--central_a0=1.44 \
		--central_P=68023.7 \
		--central_orbitTp=$O2_START \
		--block_size 345600 \
		--ignore_wings 41728 \
		--out_prefix cjs_gpu_out/outignored \
		--obs_len 10 \
		--null_atoms 2 &> /dev/null ; then
		RESULT="FAILED"
	else
		RESULT="passed"
	fi
	printf "Test (%27s): %-70s: %s\n" "$TO_TEST" "error if --null_atoms < allowed number" "$RESULT"

	if ../bin/$TO_TEST \
		--cjs_atoms=atoms/the-%02d.atoms \
		--start_time=$O2_START \
		--tblock 86400 \
		--central_a0=1.44 \
		--central_P=68023.7 \
		--central_orbitTp=$O2_START \
		--block_size 345600 \
		--ignore_wings 41728 \
		--out_prefix cjs_gpu_out/outignored \
		--obs_len 10 \
		--null_atoms 4 &> /dev/null ; then
		RESULT="FAILED"
	else
		RESULT="passed"
	fi
	printf "Test (%27s): %-70s: %s\n" "$TO_TEST" "error if --null_atoms > allowed number" "$RESULT"

}

cleanup() {
	rm -r fakedata atoms{,_withnull} cjs_gpu_out
}

echo "Setting up..."
make_data
run_cjs

echo "Testing..."
mkdir -p cjs_gpu_out
echo
test_simpleerrors viterbi_jstat_search
test_sameasnull viterbi_jstat_search
test_simpleerrors viterbi_jstat_search_single
test_sameasnull viterbi_jstat_search_single

echo "Cleaning up..."
cleanup
