#include <string.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <complex.h>
#include <glob.h>
#include <libgen.h>
#include <cuda.h>
#include <cufft.h>
#include <cuComplex.h>

#include "viterbi.h"

#define BLOCK_EXTRA 128

__global__ void bessel_convolve(cuDoubleComplex *fstat_transform_data, cuDoubleComplex *filter, int transform_block_size, int block_size)
{
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    if(idx >= transform_block_size)
    {
        return;
    }
    fstat_transform_data[idx] = cuCmul(fstat_transform_data[idx], filter[idx]);
    fstat_transform_data[idx] = (cuDoubleComplex) {fstat_transform_data[idx].x/block_size, fstat_transform_data[idx].y/block_size};
}

double bessel_read_block(double *fstats, FILE *obs_file, int block_size, double *freqs, int start_offset)
{
    printf("Start offset is %d\n", start_offset);
    char line[1000];
    int i = 0;
    int pos = 0;
    double freq_sum = 0;
    while(i < block_size)
    {
        pos++;
        fgets(line, sizeof(line), obs_file);
        if(line[0] == '%' || pos < start_offset)
        {
            continue;
        }
        sscanf(line, "%lf %*f %*f %*f %*f %*f %lf", &freqs[i], &fstats[i]);

        freq_sum += freqs[i];
        i++;
    }
    return freq_sum / block_size;
}

__global__ void compute_filter(double *bessel_filter, double a0, double *freqs, double central_freq, double P, int M, int block_size)
{
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    /*if(idx >= M)
    {
        return;
    }
    int n = idx - ((M-1)/2);
    int comb_offset = (int)round(n/P*bins_per_hz);
    int filter_idx = (comb_offset % (block_size-1) + (block_size-1)) % (block_size-1);
    if(n < 0)
    {
        bessel_filter[filter_idx] = pow(jn(-n, 2*M_PI*central_freq*a0), 2);
    }
    if(n >= 0)
    {
        bessel_filter[filter_idx] = pow(jn(n, 2*M_PI*central_freq*a0), 2);
    }*/
    if(idx > (M-1)/2)
    {
        return;
    }
    double freq_above = central_freq + idx/P;
    double freq_below = central_freq - idx/P;
    double bins_per_hz = block_size/(freqs[block_size-1]-freqs[0]);
    double raw_bin_above = (freq_above - freqs[0])*bins_per_hz;
    double raw_bin_below = (freq_below - freqs[0])*bins_per_hz;
    int comb_offset_above, comb_offset_below;


    comb_offset_above = (int)round(raw_bin_above);
    comb_offset_below = (int)round(raw_bin_below);

    if(fabs(freqs[comb_offset_above-1] - (freq_above)) < fabs(freqs[comb_offset_above] - (freq_above)))
        comb_offset_above--;

    if(fabs(freqs[comb_offset_above+1] - (freq_above)) < fabs(freqs[comb_offset_above] - (freq_above)))
        comb_offset_above++;

    if(fabs(freqs[comb_offset_below-1] - (freq_below)) < fabs(freqs[comb_offset_below] - (freq_below)))
        comb_offset_below--;

    if(fabs(freqs[comb_offset_below+1] - (freq_below)) < fabs(freqs[comb_offset_below] - (freq_below)))
        comb_offset_below++;


    /*bessel_filter[comb_offset_above] = pow(jn(idx, 2*M_PI*central_freq*a0), 2);
    bessel_filter[comb_offset_below] = pow(jn(idx, 2*M_PI*central_freq*a0), 2);*/
    bessel_filter[comb_offset_above] = jn(idx, 2*M_PI*central_freq*a0)*jn(idx, 2*M_PI*central_freq*a0);
    bessel_filter[comb_offset_below] = bessel_filter[comb_offset_above];
}


int main(int argc, char **argv)
{
    opterr = 0;
    static struct option long_options[] =
    {
        {"fstats", required_argument, 0, 0},
        {"central_a0", required_argument, 0, 0},
        {"a0_band", required_argument, 0, 0},
        {"a0_bins", required_argument, 0, 0},
        {"P", required_argument, 0, 0},
        {"gpu", required_argument, 0, 0},
        {"num_blocks", required_argument, 0, 0},
        {"block_size", required_argument, 0, 0},
        {"start_offset", required_argument, 0, 0},
        {"out", required_argument, 0, 0},
        {0,0,0,0}
    };

    char *fstats_dir;
    double central_a0;
    double a0_band;
    int a0_bins;
    double P;
    int num_freqs;
    int gpu_sel;
    int num_blocks;
    int block_size;
    int start_offset = 0;
    char *out_prefix;
    int padded_block_size;
    int padded_transform_block_size;

    double bottom_freq, top_freq, central_freq;
    cudaError_t err;

    char c;
    while(1)
    {
        int option_index = 0;
        c = getopt_long(argc, argv, "", long_options, &option_index);
        if (c == -1)
            break;

        switch (c)
        {
        case 0:
            switch (option_index)
            {
            case 0:
                fstats_dir = (char *)malloc(strlen(optarg)+1);
                strcpy(fstats_dir, optarg);
                printf("fstats directory is %s\n", fstats_dir);
                break;
            case 1:
                sscanf(optarg, "%lf", &central_a0);
                printf("central a0 is %lf\n", central_a0);
                break;
            case 2:
                sscanf(optarg, "%lf", &a0_band);
                printf("a0 band is %lf\n", a0_band);
                break;
            case 3:
                sscanf(optarg, "%d", &a0_bins);
                printf("a0 bins is %d\n", a0_bins);
                break;
            case 4:
                sscanf(optarg, "%lf", &P);
                printf("P is %lf\n", P);
                break;
            case 5:
                sscanf(optarg, "%d", &gpu_sel);
                printf("GPU to use is %d\n", gpu_sel);
                break;
            case 6:
                sscanf(optarg, "%d", &num_blocks);
                printf("Number of blocks is %d\n", num_blocks);
                break;
            case 7:
                sscanf(optarg, "%d", &block_size);
                printf("Block size is %d\n", block_size);
                padded_block_size = block_size;
                padded_transform_block_size = padded_block_size/2 + 1;
                printf("%d\n", padded_block_size);
                break;
            case 8:
                sscanf(optarg, "%d", &start_offset);
                printf("Start offset is %d\n", start_offset);
                break;
            case 9:
                out_prefix = (char *)malloc(strlen(optarg)+1);
                strcpy(out_prefix, optarg);
                printf("out prefix is %s\n", out_prefix);
            }
        }
    }

    char *fstats_glob_pattern = (char *)malloc(strlen(fstats_dir)+100);
    sprintf(fstats_glob_pattern, "%s/fstat_*.dat", fstats_dir);
    glob_t fstats_glob;
    glob(fstats_glob_pattern, GLOB_ERR, NULL, &fstats_glob);

    int obs_len = fstats_glob.gl_pathc;
    //int obs_len = 1;
    printf("Obs len is %d\n", obs_len);
    int deviceIDCount = 1;

    double *fstat_data = (double *)malloc(sizeof(double)*block_size*obs_len);
    double **bessel_fstat_gpu_data = (double **)malloc(sizeof(double *)*deviceIDCount);
    cuDoubleComplex **fstat_transform_gpu_data = (cuDoubleComplex **)malloc(sizeof(cuDoubleComplex *)*deviceIDCount);
    double **filter_gpu_data = (double **)malloc(sizeof(double *)*deviceIDCount);
    cuDoubleComplex **filter_transform_gpu_data = (cuDoubleComplex **)malloc(sizeof(cuDoubleComplex *)*deviceIDCount);

    trellis_entry_t **trellis_gpu_data = (trellis_entry_t **)malloc(sizeof(trellis_entry_t *)*deviceIDCount);

    double *bessel_filter = (double *)malloc(sizeof(double)*block_size);

    double *fstat_gpu_data;
    err = cudaMalloc(&fstat_gpu_data, sizeof(double)*padded_block_size);

    double *freqs_gpu_data;
    err = cudaMalloc(&freqs_gpu_data, sizeof(double)*block_size);

    for(int device = 0; device < deviceIDCount; device++)
    {
        cudaSetDevice(device);
        cudaMalloc((void **)&bessel_fstat_gpu_data[device], (size_t)(block_size*sizeof(double)*obs_len));
        err = cudaMalloc((void **)&fstat_transform_gpu_data[device], (size_t)(padded_transform_block_size*obs_len*sizeof(cuDoubleComplex)));
        cudaMalloc((void **)&filter_gpu_data[device], (size_t)(padded_block_size*sizeof(double)));
        cudaMalloc((void **)&filter_transform_gpu_data[device], (size_t)(padded_transform_block_size*sizeof(cuDoubleComplex)));
        
        cudaMalloc((void **)&trellis_gpu_data[device], (size_t)(block_size*obs_len*sizeof(trellis_entry_t)));
    }

    cufftHandle fstat_plan;
    if(cufftPlan1d(&fstat_plan, padded_block_size, CUFFT_D2Z, 1) != CUFFT_SUCCESS)
        printf("fstat_plan creation failed!\n");
    printf("Reading Fstat data\n");
    double *freqs = (double *) malloc(sizeof(double)*block_size);
    for(int i = 0; i < obs_len; i++)
    {
        printf("%s\n", fstats_glob.gl_pathv[i]);
    }
    for(int i = 0; i < obs_len; i++)
    {
        FILE *fstat_file = fopen(fstats_glob.gl_pathv[i],"r");

        int file_number = 0;
        char *file_name = basename(fstats_glob.gl_pathv[i]);
        sscanf(file_name, "fstat_%d.dat", &file_number);
        printf("FILENAME: %s\n",file_name);
        printf("File number: %d\n", file_number);

        central_freq = bessel_read_block(&(fstat_data[file_number*block_size]), fstat_file, block_size, freqs, start_offset);
        printf("%.16lf %.16lf %.16lf\n", freqs[0], central_freq, freqs[block_size-1]);
        fclose(fstat_file);

        cudaMemset(fstat_gpu_data, 0, sizeof(double)*padded_block_size);
        cudaMemcpy((void *)fstat_gpu_data, (void *)&(fstat_data[file_number*block_size]), block_size*sizeof(double), cudaMemcpyHostToDevice);

        if(cufftExecD2Z(fstat_plan, fstat_gpu_data, fstat_transform_gpu_data[0] + file_number*padded_transform_block_size) != CUFFT_SUCCESS)
            printf("Execution failed!\n");
        cudaDeviceSynchronize();
    }
    cudaFree(fstat_gpu_data);
    cufftDestroy(fstat_plan);

    cudaMemcpy(freqs_gpu_data, freqs, sizeof(double)*block_size, cudaMemcpyHostToDevice);

    cuDoubleComplex **transform_scratch_gpu_data = (cuDoubleComplex **)malloc(sizeof(cuDoubleComplex *)*deviceIDCount);
    err = cudaMalloc((void **)&transform_scratch_gpu_data[0], (size_t)(padded_block_size*sizeof(cuDoubleComplex)));

    double max_a0 = -INFINITY;
    double max_loglike = -INFINITY;
    double max_score = -INFINITY;
    int *max_path = (int *)malloc(sizeof(int)*obs_len);
    trellis_entry_t *best_trellis = (trellis_entry_t *)malloc(sizeof(trellis_entry_t)*block_size);

    FILE *a0_scores_loglikes = fopen("a0_scores_loglikes.dat","w");
    for(int a0_bin = 0; a0_bin < a0_bins; a0_bin++)
    {
        double a0 = central_a0 - (a0_band/2) + ((double)a0_bin/a0_bins)*a0_band;
        printf("a0 = %lf\n", a0);
        printf("a0 bin is %d\n", a0_bin);
        int M = 2*(int)ceil(2*M_PI*central_freq*a0)+201;
        printf("M is %d\n", M);
        err = cudaMemset(filter_gpu_data[0], 0, sizeof(double)*padded_block_size);
        compute_filter<<<((M-1)/2+255)/256, 256>>>(filter_gpu_data[0], a0, freqs_gpu_data, central_freq, P, M, padded_block_size);
        /*double *filter_ret = (double *)malloc(sizeof(double)*block_size);
        cudaMemcpy(filter_ret, filter_gpu_data[0], sizeof(double)*block_size, cudaMemcpyDeviceToHost);
        FILE *filter_dump = fopen("filter_dump.dat","w");
        for(int i = 0; i < block_size; i++)
        {
            fprintf(filter_dump, "%.16lf\n", filter_ret[i]);
        }
        fclose(filter_dump);*/

        cufftHandle filter_plan;
        if(cufftPlan1d(&filter_plan, padded_block_size, CUFFT_D2Z, 1) != CUFFT_SUCCESS)
            printf("filter_plan creation failed!\n");

        if(cufftExecD2Z(filter_plan, filter_gpu_data[0], filter_transform_gpu_data[0]) != CUFFT_SUCCESS)
            printf("Execution failed!\n");
        cufftResult destroy_res = cufftDestroy(filter_plan);
        err = cudaDeviceSynchronize();
        printf("Computing bessel Fstat\n");
        for(int i = 0; i < obs_len; i++)
        {
            err = cudaMemcpy(transform_scratch_gpu_data[0], fstat_transform_gpu_data[0] + i*padded_transform_block_size, sizeof(cuDoubleComplex)*padded_transform_block_size, cudaMemcpyDeviceToDevice);
            bessel_convolve<<<(padded_transform_block_size+255)/256, 256>>>(transform_scratch_gpu_data[0], filter_transform_gpu_data[0], padded_transform_block_size, padded_block_size);
            err = cudaPeekAtLastError();

            cufftHandle fstat_inverse_plan;
            cufftResult res;
            if((res = cufftPlan1d(&fstat_inverse_plan, padded_block_size, CUFFT_Z2D, 1)) != CUFFT_SUCCESS)
                printf("fstat_inverse_plan creation failed!\n");

            if(cufftExecZ2D(fstat_inverse_plan, transform_scratch_gpu_data[0], (double *)transform_scratch_gpu_data[0])  != CUFFT_SUCCESS)
                printf("Execution failed!\n");
            err = cudaMemcpy(bessel_fstat_gpu_data[0] + i*block_size, (double *)transform_scratch_gpu_data[0] + block_size/2, sizeof(double)*(block_size/2), cudaMemcpyDeviceToDevice);
            err = cudaMemcpy(bessel_fstat_gpu_data[0] + i*block_size + block_size/2, transform_scratch_gpu_data[0], sizeof(double)*(block_size/2), cudaMemcpyDeviceToDevice);
//            err = cudaMemcpy(bessel_fstat_gpu_data[0]+i*block_size, transform_scratch_gpu_data[0], sizeof(double)*(block_size), cudaMemcpyDeviceToDevice);

            cufftDestroy(fstat_inverse_plan);

/*            if(i == obs_len-1)
            {
                double *ret = (double *)malloc(sizeof(double)*block_size);
                cudaMemcpy(ret, bessel_fstat_gpu_data[0] + (20)*block_size, sizeof(double)*block_size, cudaMemcpyDeviceToHost);
                FILE *dump = fopen("bfstat_dump.dat","w");
                for(int j = 0; j < block_size; j++)
                    fprintf(dump, "%.16lf\n", ret[j]);
                fclose(dump);
            }*/
        }
        viterbi_run(bessel_fstat_gpu_data[0], trellis_gpu_data[0], block_size, obs_len);
        int *path = (int *)malloc(sizeof(int)*obs_len);
        double score;
        double loglike = viterbi_results(trellis_gpu_data[0], block_size, obs_len, max_score, path, NULL, 0, NULL, NULL, 0, NULL, &score, best_trellis);
/*        char out_filename[100];
        sprintf(out_filename, "out-%.16lf.dat", a0);
        FILE *out_file = fopen(out_filename, "w");
        for(int j = 0; j < obs_len; j++)
        {
            fprintf(out_file, "%d\n", path[j]);
        }

        fprintf(out_file, "%.16lf\n", loglike);
        fprintf(out_file, "%.16lf\n", score);
        fclose(out_file);*/

        if(score > max_score)
        {
            max_loglike = loglike;
            max_a0 = a0;
            max_score = score;
            memcpy(max_path, path, sizeof(int)*obs_len);
        }

        free(path);
        fprintf(a0_scores_loglikes, "%.16lf %.16lf %.16lf\n", a0, score, loglike);
    }
    fclose(a0_scores_loglikes);
    double recovered_freq = 0;
    double bins_per_hz = block_size/(freqs[block_size-1] - freqs[0]);
    char  path_filename[100];
    sprintf(path_filename, "%s_path.dat", out_prefix);
    FILE *path_file = fopen(path_filename , "w");
    for(int j = 0; j < obs_len; j++)
    {
        fprintf(path_file, "%.16lf\n", freqs[0] + (double)max_path[j]/bins_per_hz);
        recovered_freq += freqs[0] + (double)max_path[j]/bins_per_hz;
    }
    fclose(path_file);

    char scores_filename[100];
    sprintf(scores_filename, "%s_scores.dat",  out_prefix);
    FILE *scores_file = fopen(scores_filename, "w");
    double loglike_mean = 0;
    double loglike_variance = 0;
    for(int i = 0; i < block_size; i++)
    {
        loglike_mean += best_trellis[i].loglike/block_size;
    }
    for(int i = 0; i < block_size; i++)
    {
        loglike_variance += pow(best_trellis[i].loglike - loglike_mean, 2)/block_size;
    }
    for(int i = 0; i < block_size; i++)
    {
        fprintf(scores_file, "%.16lf\n", (best_trellis[i].loglike - loglike_mean)/sqrt(loglike_variance));
    }

    FILE *bfstat_dump = fopen("bfstat_dump.dat", "w");
    for(int i = 0; i < block_size; i++)
        fprintf(bfstat_dump, "%.16lf\n", best_trellis[i].loglike);
    fclose(bfstat_dump);


    recovered_freq /= obs_len;
    printf("Loglike mean: %.16lf\n", loglike_mean);
    printf("Loglike variance: %.16lf\n", loglike_variance);
    printf("Max a0: %.16lf\n", max_a0);
    printf("Max loglike: %.16lf\n", max_loglike);
    printf("Max score: %.16lf\n", max_score);
    printf("Recovered frequency: %.16lf\n", recovered_freq);
}
