#include <cuda.h>
#include <stdio.h>
#include "viterbi.h"

__global__ void init(double *obs_loglike, trellis_entry_t *trellis, int block_size)
{
    int idx = threadIdx.x + blockDim.x*blockIdx.x;
    if(idx >= block_size)
    {
        return;
    }
    trellis[idx] = (trellis_entry_t) { obs_loglike[idx], -1};
}

__global__ void viterbi(double *this_step_obs_loglike, trellis_entry_t *trellis, int trellis_block_size, int t)
{
    trellis_entry_t *prev = &trellis[(t-1)*trellis_block_size];
    trellis_entry_t *result = &trellis[t*trellis_block_size];

    int idx = threadIdx.x + blockDim.x*blockIdx.x;
    if(idx >= trellis_block_size)
    {
        return;
    }
    if(idx == 0)
    {
        const double trans_loglike = log(1.0/3.0);
        double loglike[2] = {-INFINITY, -INFINITY};
        loglike[0] = prev[0].loglike +  this_step_obs_loglike[0] + trans_loglike;
        loglike[1] = prev[1].loglike +  this_step_obs_loglike[0] + trans_loglike;
        if (loglike[0] > loglike[1])
            result[idx] = (trellis_entry_t) {loglike[0], 0};
        else
            result[idx] = (trellis_entry_t) {loglike[1], 1};
    }
    else if(idx == trellis_block_size-1)
    {
        const double trans_loglike = log(1.0/3.0);
        double loglike[2] = {-INFINITY, -INFINITY};
        loglike[0] = prev[idx-1].loglike +  this_step_obs_loglike[trellis_block_size-1] + trans_loglike;
        loglike[1] = prev[idx].loglike +  this_step_obs_loglike[trellis_block_size-1] + trans_loglike;
        if (loglike[0] > loglike[1])
            result[idx] = (trellis_entry_t) {loglike[0], idx-1};
        else
            result[idx] = (trellis_entry_t) {loglike[1], idx};

    }
    else
    {
        const double trans_loglikes[3] = {log(1.0/3.0), log(1.0/3.0), log(1.0/3.0)};
		/** Here's the original version of the code, if you want to support a
		 * larger transition matrix:
        for(int i = idx-1; i <= idx+1; i++)
        {
            loglike[i-(idx-1)] = prev[i].loglike +  this_step_obs_loglike[idx] + trans_loglikes[i-(idx-1)];
        }
		 * However, unrolling the loop works wonders here.
		*/
		double loglike[3] = {
			loglike[0] = prev[idx-1].loglike + this_step_obs_loglike[idx] + trans_loglikes[0],
			loglike[1] = prev[idx  ].loglike + this_step_obs_loglike[idx] + trans_loglikes[1],
			loglike[2] = prev[idx+1].loglike + this_step_obs_loglike[idx] + trans_loglikes[2]
		};
        if(loglike[0] > loglike[1] && loglike[0] > loglike[2])
            result[idx] = (trellis_entry_t) {loglike[0], idx-1};
        else if(loglike[1] > loglike[2])
            result[idx] = (trellis_entry_t) {loglike[1], idx};
        else
            result[idx] = (trellis_entry_t) {loglike[2], idx+1};
    }
}

__global__ void best_paths(trellis_entry_t *trellis_final_step, int *best_paths_idx, trellis_entry_t *best_paths_trellis, int block_size)
{
    __shared__ trellis_entry_t shared_trellis[256];
    __shared__ int shared_idxs[256];
    int thread_idx = threadIdx.x;
    int global_idx = blockIdx.x*blockDim.x + threadIdx.x;
    
    if(global_idx >= block_size)
    {
        shared_trellis[thread_idx] = (trellis_entry_t) { -INFINITY, -1 };
        shared_idxs[thread_idx] = -1;
    }
    else
    {
        shared_trellis[thread_idx] = (trellis_entry_t) {trellis_final_step[global_idx].loglike,
                                                        trellis_final_step[global_idx].backptr}; 
        shared_idxs[thread_idx] = global_idx;
    }
    __syncthreads();

    for(int s = 1; s < blockDim.x; s *= 2)
    {
        if(thread_idx % (2*s) == 0)
        {
            if(shared_trellis[thread_idx + s].loglike > shared_trellis[thread_idx].loglike)
            {
                shared_trellis[thread_idx] = (trellis_entry_t) {shared_trellis[thread_idx + s].loglike, 
                                                            shared_trellis[thread_idx + s].backptr};
                shared_idxs[thread_idx] = shared_idxs[thread_idx + s];
            }
        }
        __syncthreads();
    }
    
    if(thread_idx == 0)
    {
        best_paths_idx[blockIdx.x] = shared_idxs[0];
        best_paths_trellis[blockIdx.x] = shared_trellis[0];
    }
}

__global__ void loglike_sum(trellis_entry_t *trellis_final_step, double *local_sums, int block_size)
{
    __shared__ double shared_sum[256];

    int thread_idx = threadIdx.x;
    int global_idx = threadIdx.x + blockIdx.x*blockDim.x;
    if(global_idx >= block_size)
    {
        shared_sum[thread_idx] = 0;
    }
    else
    {
        shared_sum[thread_idx] = trellis_final_step[global_idx].loglike;
    }
    __syncthreads();

    for(int s = 1; s < blockDim.x; s *= 2)
    {
        if(thread_idx % (2*s) == 0)
        {
            shared_sum[thread_idx] += shared_sum[thread_idx + s];
        }
        __syncthreads();
    }

    if(thread_idx == 0)
    {
        local_sums[blockIdx.x] = shared_sum[0];
    }
}

__global__ void gpu_loglike_variance(trellis_entry_t *trellis_final_step, double mean, double *local_sums, int block_size)
{
    __shared__ double shared_sum[256];

    int thread_idx = threadIdx.x;
    int global_idx = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(global_idx >= block_size)
    {
        shared_sum[thread_idx] = 0;
    }
    else
    {
        shared_sum[thread_idx] = pow(trellis_final_step[global_idx].loglike - mean, 2);
    }
    __syncthreads();

    for(int s = 1; s < blockDim.x; s *= 2)
    {
        if(thread_idx % (2*s) == 0)
        {
            shared_sum[thread_idx] += shared_sum[thread_idx + s];
        }
        __syncthreads();
    }

    if(thread_idx == 0)
    {
        local_sums[blockIdx.x] = shared_sum[0];
    }
}

__global__ void backtrace(trellis_entry_t *trellis, int best_path_idx, int block_size, int obs_len, int *path)
{
    path[obs_len-1] = best_path_idx;

    for(int i = obs_len-2; i >= 0; i--)
    {
        path[i] = trellis[(i+1)*block_size + path[i+1]].backptr;
    }
}

/** Run the Viterbi search;
 * obs_loglike_gpu_data  -- observed log likelihoods (i.e. J/F-statistic)
 * trellis_gpu_data      -- where to put the output
 * ol_block_size         -- # of loglikelihood frequency bins
 * trellis_block_size    -- # of trellis frequency bins
 * ol_offset             -- how far in to the loglikelihood array to start
 * obs_len               -- # of timesteps
 */
void viterbi_run(double *obs_loglike_gpu_data, trellis_entry_t *trellis_gpu_data, int ol_block_size, int trellis_block_size, int ol_offset, int obs_len)
{
	// First block of trellis_gpu_data should be equal to first obslikelihood
    init<<<(trellis_block_size+255)/256, 256>>>(obs_loglike_gpu_data + ol_offset, trellis_gpu_data, trellis_block_size);
	// Now go through each timestep
    for(int i = 1; i < obs_len; i++)
    {
        viterbi<<<(trellis_block_size+255)/256, 256>>>(obs_loglike_gpu_data + (ol_block_size * i) + ol_offset, trellis_gpu_data, trellis_block_size, i);
    }
}

double viterbi_results(
		trellis_entry_t *trellis_gpu_data,
		int block_size,
		int obs_len,
		double best_score,
		int *path,
		double *sampled_scores,
		int num_sampled_scores,
		int **paths_above_threshold,
		double **path_scores_above_threshold,
		double threshold,
		int *num_threshold_scores_returned,
		double *score,
		int *num_llthreshold_scores_returned,
		double **lls_above_threshold,
		double llthreshold,
		trellis_entry_t *best_trellis)
{
    int *best_paths_idx_gpu_data, *best_paths_idx_ret;
    trellis_entry_t *best_paths_trellis_gpu_data, *best_paths_trellis_ret;
    int *final_path_gpu_data;

    best_paths_idx_ret = (int *)malloc(((block_size + 255)/256)*sizeof(int));
    best_paths_trellis_ret = (trellis_entry_t *)malloc(((block_size + 255)/256)*sizeof(trellis_entry_t));

    cudaMalloc(&best_paths_idx_gpu_data, (size_t)(((block_size + 255)/256)*sizeof(int)));
    cudaMalloc(&best_paths_trellis_gpu_data, (size_t)(((block_size + 255)/256)*sizeof(trellis_entry_t)));
    cudaMalloc(&final_path_gpu_data, (size_t)(obs_len*sizeof(int)));

    best_paths<<<(block_size + 255)/256, 256>>>(trellis_gpu_data + (obs_len - 1)*block_size, best_paths_idx_gpu_data, best_paths_trellis_gpu_data, block_size);

    cudaMemcpy(best_paths_idx_ret, best_paths_idx_gpu_data, ((block_size + 255)/256)*sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(best_paths_trellis_ret, best_paths_trellis_gpu_data, ((block_size + 255)/256)*sizeof(trellis_entry_t), cudaMemcpyDeviceToHost);

	static double loglike_mean = 0;
	if(1 || loglike_mean == 0) {
		double *loglike_mean_sums_gpu_data, *loglike_mean_sums_ret;
		loglike_mean_sums_ret = (double *)malloc(((block_size + 255)/256)*sizeof(double));
		cudaMalloc(&loglike_mean_sums_gpu_data, (size_t)(((block_size + 255)/256)*sizeof(double)));
		loglike_sum<<<(block_size + 255)/256, 256>>>(trellis_gpu_data + (obs_len - 1)*block_size, loglike_mean_sums_gpu_data, block_size);
		cudaMemcpy(loglike_mean_sums_ret, loglike_mean_sums_gpu_data, ((block_size + 255)/256)*sizeof(double), cudaMemcpyDeviceToHost);

		for(int i = 0; i < (block_size + 255)/256; i++)
		{
			loglike_mean += loglike_mean_sums_ret[i];
		}
		loglike_mean /= block_size;
		free(loglike_mean_sums_ret);
		cudaFree(loglike_mean_sums_gpu_data);
	}

    static double loglike_variance = 0;
	if(1 || loglike_variance == 0) {
		double *loglike_variance_sums_gpu_data, *loglike_variance_sums_ret;
		loglike_variance_sums_ret = (double *)malloc(((block_size + 255)/256)*sizeof(double));
		cudaMalloc(&loglike_variance_sums_gpu_data, (size_t)(((block_size + 255)/256)*sizeof(double)));
		gpu_loglike_variance<<<(block_size + 255)/256,  256>>>(trellis_gpu_data + (obs_len - 1)*block_size, loglike_mean, loglike_variance_sums_gpu_data, block_size);
		cudaMemcpy(loglike_variance_sums_ret, loglike_variance_sums_gpu_data, ((block_size + 255)/256)*sizeof(double), cudaMemcpyDeviceToHost);

		for(int i = 0; i < (block_size + 255)/256; i++)
		{
			loglike_variance += loglike_variance_sums_ret[i];
		}
		loglike_variance /= block_size;
		free(loglike_variance_sums_ret);
		cudaFree(loglike_variance_sums_gpu_data);
	}

    int best_path_idx = 0;
    double max_loglike = -INFINITY;
    for(int i = 0; i < (block_size + 255)/256; i++)
    {
        if(best_paths_trellis_ret[i].loglike > max_loglike)
        {
            max_loglike = best_paths_trellis_ret[i].loglike;
            best_path_idx = best_paths_idx_ret[i];
        }
    }
    
    backtrace<<<1,1>>>(trellis_gpu_data, best_path_idx, block_size, obs_len, final_path_gpu_data);

    cudaMemcpy(path, final_path_gpu_data, obs_len*sizeof(int), cudaMemcpyDeviceToHost);

    if(sampled_scores != NULL || paths_above_threshold != NULL)
    {
        //Randomly sample the Viterbi scrores of 50 other paths
        static trellis_entry_t *trellis_final_step = (trellis_entry_t *)malloc(sizeof(trellis_entry_t)*block_size); // Static to avoid reallocating this every loop iteration
        cudaMemcpy(trellis_final_step, trellis_gpu_data + (obs_len-1)*block_size, sizeof(trellis_entry_t)*block_size, cudaMemcpyDeviceToHost);
        if(sampled_scores != NULL)
        {
            srand(time(NULL));
            for(int i = 0; i < num_sampled_scores; i++)
            {
                int path_idx = rand() % block_size;
                double loglike = trellis_final_step[path_idx].loglike;
                double score = (loglike - loglike_mean)/sqrt(loglike_variance);
                sampled_scores[i] = score;
            }
        }

        if(paths_above_threshold != NULL || lls_above_threshold != NULL) {
            // Find the (first 100) paths above the threshold
            size_t current_path_score = 0;
			size_t current_path_ll = 0;
			size_t allocated_memory_score = 0;
			size_t allocated_memory_ll = 0;
            for(size_t i = 0; i < (size_t)block_size; ++i)
            {
                const double loglike = trellis_final_step[i].loglike;
                const double score = (loglike - loglike_mean)/sqrt(loglike_variance);
                if(paths_above_threshold && score > threshold) {
					// (re)allocate enough memory
					if(current_path_score >= allocated_memory_score) {
						if(allocated_memory_score == 0) {
							allocated_memory_score = 2;
						} else {
							allocated_memory_score *= 2;
						}
						*paths_above_threshold = (int*)realloc(*paths_above_threshold, sizeof(int) * obs_len * allocated_memory_score);
						*path_scores_above_threshold = (double*)realloc(*path_scores_above_threshold, sizeof(double) * allocated_memory_score);
					}
                    // Copy the score into path_scores; and the path into paths_a_t
                    (*path_scores_above_threshold)[current_path_score] = score;
                    // We can re-use the (GPU) memory that the best path was saved in
                    backtrace<<<1,1>>>(trellis_gpu_data, i, block_size, obs_len, final_path_gpu_data);
                    cudaMemcpy( (*paths_above_threshold) + (current_path_score * obs_len), final_path_gpu_data, obs_len*sizeof(int), cudaMemcpyDeviceToHost);
                    ++current_path_score;
                }

				if(lls_above_threshold && loglike > llthreshold) {
					if(current_path_ll >= allocated_memory_ll) {
						if(allocated_memory_ll == 0) {
							allocated_memory_ll = 2;
						} else {
							allocated_memory_ll *= 2;
						}
						*lls_above_threshold = (double*)realloc(*lls_above_threshold, sizeof(double) * allocated_memory_ll);
					}

					(*lls_above_threshold)[current_path_ll] = loglike;
					// In this branch, we don't save the path

                    ++current_path_ll;
				}
            }
            *num_threshold_scores_returned = current_path_score;
			*num_llthreshold_scores_returned = current_path_ll;
        }
    }

    free(best_paths_idx_ret);
    free(best_paths_trellis_ret);
    cudaFree(best_paths_idx_gpu_data);
    cudaFree(best_paths_trellis_gpu_data);
    if(score != NULL)
    {
        *score = (max_loglike - loglike_mean)/sqrt(loglike_variance);
		if(*score > best_score)
		{
			if(best_trellis != NULL) {
				trellis_entry_t *trellis_last_step_ret = (trellis_entry_t *)malloc(sizeof(trellis_entry_t)*block_size);
				cudaMemcpy(trellis_last_step_ret, trellis_gpu_data + (obs_len-1)*block_size, sizeof(trellis_entry_t)*block_size, cudaMemcpyDeviceToHost);
				memcpy(best_trellis, trellis_last_step_ret, sizeof(trellis_entry_t)*block_size);
				free(trellis_last_step_ret);
			}
		}
    }
    return max_loglike;
}
