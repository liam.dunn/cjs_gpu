#include <string.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <complex.h>
#include <glob.h>
#include <libgen.h>
#include <cuda.h>
#include <cufft.h>
#include <cuComplex.h>

#include "viterbi.h"

void read_block(double *fstats, FILE *obs_file, int block_size)
{
    char line[1000];
    int i = 0;
    while(i < block_size)
    {
        fgets(line, sizeof(line), obs_file);
        if(line[0] == '%')
            continue;
        sscanf(line, "%*lf %*f %*f %*f %*f %*f %lf", &fstats[i]);
        i++;
    }
}
int main(int argc, char **argv)
{
    opterr = 0;
    static struct option long_options[] =
    {
        {"fstats", required_argument, 0, 0},
        {"num_sampled_scores", required_argument, 0, 0},
        {"block_size", required_argument, 0, 0},
        {"out_prefix", required_argument, 0, 0},
        {"score_threshold", required_argument, 0, 0},
        {0,0,0,0}
    };

    char *fstats_dir;
    int num_sampled_scores;
    int block_size;
    char *out_prefix;
    double score_threshold = 0.;

    char c;
    while(1)
    {
        int option_index = 0;
        c = getopt_long(argc, argv, "", long_options, &option_index);
        if (c == -1)
            break;

        switch (c)
        {
        case 0:
            switch (option_index)
            {
            case 0:
                fstats_dir = (char *)malloc(strlen(optarg)+1);
                strcpy(fstats_dir, optarg);
                printf("fstats directory is %s\n", fstats_dir);
                break;
            case 1:
                sscanf(optarg, "%d", &num_sampled_scores);
                printf("Number of scores to sample is %d\n", num_sampled_scores);
                break;
            case 2:
                sscanf(optarg, "%d", &block_size);
                printf("Block size is %d\n", block_size);
                break;
            case 3:
                out_prefix = (char *)malloc(strlen(optarg)+1);
                strcpy(out_prefix, optarg);
                printf("Out prefix is %s\n", out_prefix);
                break;
            case 4:
                if(sscanf(optarg, "%lf", &score_threshold) != 1) {
                    fprintf(stderr, "Couldn't parse '%s' as a number in --score_threshold, bailing out\n", optarg);
                    return 1;
                }
                printf("Score threshold is %f\n", score_threshold);
                break;
            }
        }
    }

    char *fstats_glob_pattern = (char *)malloc(strlen(fstats_dir)+100);
    sprintf(fstats_glob_pattern, "%s/fstat_*.dat", fstats_dir);
    glob_t fstats_glob;
    glob(fstats_glob_pattern, GLOB_ERR, NULL, &fstats_glob);

    int obs_len = fstats_glob.gl_pathc;
    int deviceIDCount = 1;

    double *fstat_data = (double *)malloc(sizeof(double)*block_size*obs_len);
    double **fstat_gpu_data = (double **)malloc(sizeof(double *)*deviceIDCount);

    trellis_entry_t **trellis_gpu_data = (trellis_entry_t **)malloc(sizeof(trellis_entry_t *)*deviceIDCount);

    for(int device = 0; device < deviceIDCount; device++)
    {
        cudaSetDevice(device);
        cudaMalloc((void **)&fstat_gpu_data[device], (size_t)(block_size*sizeof(double)*obs_len));
        cudaMalloc((void **)&trellis_gpu_data[device], (size_t)(block_size*obs_len*sizeof(trellis_entry_t)));
    }

    for(int i = 0; i < obs_len; i++)
    {
        printf("%s\n", fstats_glob.gl_pathv[i]);
        FILE *fstat_file = fopen(fstats_glob.gl_pathv[i],"r");

        int file_number = 0;
        char *file_name = basename(fstats_glob.gl_pathv[i]);
        sscanf(file_name, "fstat_%d.dat", &file_number);

        read_block(&(fstat_data[file_number*block_size]), fstat_file, block_size);

        cudaMemcpyAsync((void *)(fstat_gpu_data[0] + file_number*block_size), (void *)&(fstat_data[file_number*block_size]), block_size*sizeof(double), cudaMemcpyHostToDevice);
    }
    cudaDeviceSynchronize();
    free(fstat_data);
    viterbi_run(fstat_gpu_data[0], trellis_gpu_data[0], block_size, obs_len);
    int *path = (int *)malloc(sizeof(int)*obs_len);
    double *sampled_scores = (num_sampled_scores > 0) ? (double *)malloc(sizeof(double)*num_sampled_scores) : NULL; 
    // Allocate space for, say, 100 paths greater than the threshold
    int *paths_above_threshold = NULL;
    double *path_scores = NULL;
    int threshold_scores_returned = 0;
    if(score_threshold > 1e-10) {
        paths_above_threshold = (int *)malloc(sizeof(int)*obs_len*100);
        path_scores = (double *)malloc(sizeof(double)*100);
    }

    double score;
    //viterbi_results(trellis_gpu_data[0], block_size, obs_len, 0, path, sampled_scores, num_sampled_scores, paths_above_threshold, path_scores, score_threshold, &threshold_scores_returned, &score, NULL);
    viterbi_results(trellis_gpu_data[0], block_size, obs_len, 0, path, NULL, num_sampled_scores, NULL, path_scores, score_threshold, &threshold_scores_returned, &score, NULL);
    char out_path_filename[1000], out_scores_filename[1000];
    sprintf(out_path_filename, "%s_path.dat", out_prefix);
    sprintf(out_scores_filename, "%s_scores.dat", out_prefix);

    FILE *out_path_file = fopen(out_path_filename, "w");
    for(int j = 0; j < obs_len; j++)
    {
        fprintf(out_path_file, "%d\n", path[j]);
    }
    printf("Viterbi score: %.16lf\n", score);
    fprintf(out_path_file, "%.16lf\n", score);
    fclose(out_path_file);
    if(num_sampled_scores > 0)
    {
        FILE *out_scores_file = fopen(out_scores_filename, "w");
        for(int j = 0; j < num_sampled_scores; j++)
        {
            fprintf(out_scores_file, "%.16lf\n", sampled_scores[j]);
        }
        fclose(out_scores_file);
    }

    if(score_threshold > 1e-10) {
        for(size_t i = 0; i < threshold_scores_returned; ++i) {
            char filename[1000];
            sprintf(filename, "%s_threshold_path_%lu.dat", out_prefix, i);
            FILE *fh = fopen(filename, "w");
            if(!fh) {
                fprintf(stderr, "Error opening %s -- skipping\n", filename);
                continue;
            }
            int *ptr = paths_above_threshold + (i * obs_len);
            for(int j = 0; j < obs_len; ++j) {
                fprintf(fh, "%d\n", *(ptr++));
            }
            fprintf(fh, "%.16lf\n", path_scores[i]);
            fclose(fh);
        }
    }

    free(path);

    free(paths_above_threshold);
    free(sampled_scores);
}
